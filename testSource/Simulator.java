/*
 * Copyright 2020 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.netbeans.quickoutline.source;

import hk.quantr.riscv_simulator.log.H2Thread;
import hk.quantr.riscv_simulator.setting.Setting;
import hk.quantr.assembler.exception.NoOfByteException;
import hk.quantr.assembler.RISCVDisassembler;
import hk.quantr.assembler.exception.WrongInstructionException;
import hk.quantr.assembler.riscv.il.Line;
import hk.quantr.assembler.riscv.il.Registers;
import hk.quantr.dwarf.QuantrDwarf;
import hk.quantr.dwarf.dwarf.Dwarf;
import hk.quantr.dwarf.dwarf.DwarfLib;
import hk.quantr.dwarf.elf.Elf_Phdr;
import hk.quantr.javalib.CommonLib;
import hk.quantr.javalib.PropertyUtil;
import hk.quantr.riscv_simulator.cpu.CPUState;
import hk.quantr.riscv_simulator.cpu.CSR;
import hk.quantr.riscv_simulator.cpu.CSRRegister;
import hk.quantr.riscv_simulator.cpu.Memory;
import hk.quantr.riscv_simulator.cpu.MemoryHandler;
import hk.quantr.riscv_simulator.cpu.MemoryMap;
import hk.quantr.riscv_simulator.cpu.PMP;
import hk.quantr.riscv_simulator.cpu.GeneralRegister;
import hk.quantr.riscv_simulator.cpu.PLIC;
import hk.quantr.riscv_simulator.cpu.Register;
import hk.quantr.riscv_simulator.cpu.UART;
import hk.quantr.riscv_simulator.exception.PageFaultException;
import hk.quantr.riscv_simulator.cpu.Virtio;
import static hk.quantr.riscv_simulator.cpu.mul128.*;

import hk.quantr.riscv_simulator.exception.AccessFaultException;
import hk.quantr.riscv_simulator.exception.IRQException;
import hk.quantr.riscv_simulator.exception.InterruptException;
import hk.quantr.riscv_simulator.exception.RiscvException;
import hk.quantr.riscv_simulator.log.FileLogThread;
import hk.quantr.riscv_simulator.log.LogInterface;
import hk.quantr.riscv_simulator.setting.FireInterrupt;
import hk.quantr.riscv_simulator.setting.MemoryHiJack;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public final class Simulator {

	private static final Logger logger = Logger.getLogger(Simulator.class.getName());
	//System structuring
	public static LinkedHashMap<String, Register> registers = new LinkedHashMap<>();
	HashSet<MemoryHandler> memoryHandlers = new HashSet<>();
	public Memory memory = new Memory(this, memoryHandlers);
	public int Harts = 1;
	//System variables
	RISCVDisassembler riscvDisassembler = new RISCVDisassembler();
	LinkedHashMap<String, Integer> bp = new LinkedHashMap<>();
	public CommandLine cmd;
	int bp_flag = 0, startAddress;
	String dumpJson;
	boolean isXml, Startcont = false, interrupt = false;
	Thread c2;
	long cpuTick = 0;
	ArrayList<Dwarf> dwarfArrayList;
	private boolean printDisassembleLine;
	public long mtime;
	public long mtimecmp;
	LogInterface logThread;

	static {
		InputStream stream = Simulator.class.getClassLoader().getResourceAsStream("logging.properties");
		try {
			LogManager.getLogManager().readConfiguration(stream);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String args[]) throws Exception {
		new Simulator(args);
	}

	public Simulator(String[] args) throws Exception {
		if (args != null) {
			CommandLineParser parser = new DefaultParser();
			Options options = new Options();
			try {
				options.addOption("v", "version", false, "display version info");
				options.addOption(Option.builder("b")
						.required(false)
						.hasArg()
						.argName("file")
						.desc("load binary file")
						.longOpt("binary")
						.build());
				options.addOption(Option.builder("e")
						.required(false)
						.hasArg()
						.argName("file")
						.desc("load elf file")
						.longOpt("elf")
						.build());
				options.addOption(Option.builder("s")
						.required(false)
						.hasArg()
						.argName("address")
						.desc("start memory address")
						.longOpt("start")
						.build());
				options.addOption(Option.builder("d")
						.required(false)
						.hasArg()
						.argName("file")
						.desc("dump each execution into json")
						.longOpt("dump")
						.build());
				options.addOption(Option.builder("f")
						.required(false)
						.hasArg()
						.argName("file")
						.desc("file system image")
						.longOpt("file")
						.build());
				options.addOption(Option.builder("i")
						.required(false)
						.hasArg()
						.argName("file")
						.desc("load init register values from file")
						.longOpt("init")
						.build());
				options.addOption(Option.builder("2")
						.required(false)
						.hasArg()
						.argName("h2 db file")
						.desc("store execution records into h2")
						.longOpt("h2")
						.build());
				options.addOption(Option.builder("g")
						.required(false)
						.hasArg()
						.argName("log file")
						.desc("store execution records into file")
						.longOpt("logfile")
						.build());
				options.addOption(Option.builder("c")
						.required(false)
						.hasArg()
						.argName("commands")
						.desc("commands separated by comma")
						.longOpt("commands")
						.build());
				options.addOption(Option.builder()
						.required(false)
						.hasArg()
						.argName("cputicks")
						.desc("only log for cpu ticks")
						.longOpt("cputicks")
						.build());
				if (Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
					HelpFormatter formatter = new HelpFormatter();
					formatter.printHelp("java -jar riscv_simulator-xx.jar [OPTION]", options);
					return;
				}

				if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--version")) {
					System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));
					return;
				}

				cmd = parser.parse(options, args);

				if (!cmd.hasOption("b") && !cmd.hasOption("e")) {
					System.out.println("Please specify --binary or --elf");
					return;
				}
				if (cmd.hasOption("cputicks")) {
					String temp = cmd.getOptionValue("cputicks");
					for (String s : temp.split(",")) {
						s = s.trim();
						if (s.contains("-")) {
							int from = Integer.parseInt(s.split("-")[0].trim());
							int to = Integer.parseInt(s.split("-")[1].trim());
							System.out.println("cpu ticks : " + from + " - " + to);
							LogTicks.data.add(Pair.of(from, to));
						}
					}
				}

				if (cmd.hasOption("dump")) {
					String temp = cmd.getOptionValue("dump");
					dumpJson = temp;
					if (new File(dumpJson).isFile()) {
						new File(dumpJson).delete();
					}

					FileUtils.writeStringToFile(new File(dumpJson), "[", "utf8", true);
				}
				if (cmd.hasOption("start")) {
					startAddress = (int) CommonLib.convertFilesize(cmd.getOptionValue("start"));
				}
				if (cmd.hasOption("init")) {
					String filename = cmd.getOptionValue("init");
					Setting setting = Setting.getInstance(filename);
				}
			} catch (ParseException | IOException ex) {
				logger.log(Level.SEVERE, ExceptionUtils.getFullStackTrace(ex));
				System.exit(1);
			}

			initRegistersAndMemory();

			if (cmd.hasOption("2")) {
				String temp = cmd.getOptionValue("h2");
				File h2 = new File(temp);
				if (h2.isFile()) {
					h2.delete();
				}
				String filenameOnly = h2.getName().split("\\.")[0];
				logThread = new H2Thread(filenameOnly);
				new Thread(logThread, "Log Thread").start();
			} else if (cmd.hasOption("logfile")) {
				logThread = new FileLogThread(cmd.getOptionValue("logfile"));
				new Thread(logThread, "Log Thread").start();
			}

			initKernel();
			initMemoryHandler();
			startSimulation();
		}
	}

	public void startSimulation() throws IOException {
		String prevCommand = "";
		int x_n = 1;
		char x_u = 'b', x_f = 'x';
		Scanner in = new Scanner(System.in);
		outer:
		while (true) {
			System.out.print(">");
			String commands[] = cmd.hasOption("commands") ? cmd.getOptionValue("commands").split(";") : in.nextLine().trim().split(";");
			try {
				for (String command : commands) {
					command = command.trim().replaceAll("( )+", " ");
					if (command.equals("q")) {
						break outer;
					}
					if (!command.isBlank()) {
						prevCommand = command;
					} else {
						command = prevCommand;
					}
					if (bp_flag == 1) {//disable breakpoint ,pc jump to next instruction
						bp_flag = 0;
					}
					if (command.equals("v")) {
						if (isXml) {
							System.out.println("<version>\n\t " + PropertyUtil.getProperty("main.properties", "version") + "\n</version>");
						} else {
							System.out.println("version : " + PropertyUtil.getProperty("main.properties", "version"));
						}
					} else if (command.equals("priv")) {
						if (isXml) {
							System.out.println(String.format("<Privilege>\n<priv>%d</priv>\n<mode>%s</mode>\n</Privilege>", CPUState.priv, CPUState.priv == 3 ? "Machine" : CPUState.priv == 2 ? "Hypervisor" : CPUState.priv == 1 ? "Supervisor" : "User"));
						} else {
							System.out.println(String.format("Privilege: %d [%s]", CPUState.priv, CPUState.priv == 3 ? "Machine" : CPUState.priv == 2 ? "Hypervisor" : CPUState.priv == 1 ? "Supervisor" : "User"));
						}
					} else if (command.matches("xml( (on|off))?")) {
						if (command.equals("xml")) {
							isXml = !isXml;
						} else if (command.split(" ")[1].equals("on")) {
							isXml = true;
						} else {
							isXml = false;
						}
						System.out.println(isXml ? "on" : "off");
					} else if (command.startsWith("r")) {
						String[] registerNames;
						if (command.equals("r")) {
							registerNames = registers.keySet().stream().filter(x -> (!x.startsWith("x") && !x.startsWith("fs") && !x.startsWith("fa") && !x.startsWith("ft"))).collect(Collectors.toList()).toArray(new String[0]);
						} else if (command.startsWith("r info")) { //command: r info [csr ...]
							String[] reg = command.replaceAll("[ ,]+", " ").substring(7).split(" ");
							for (String r : reg) {
								if (registers.get(r) == null) {
									System.out.println(String.format("%15s = %16s ", r, "Unrecognized reg"));
									continue;
								}
								System.out.println(String.format("%15s = %016x ", r, registers.get(r).getValue().longValue()));
								printRegisterInfo(r);
							}
							continue;
						} else {
							command = command.replaceAll("[ ,]+", " ").substring(2);
							registerNames = command.split(" ");
						}
						int perCol = 5;
						if (isXml) {
							System.out.println("<registers>");
							for (int x = 0; x < registerNames.length; x++) {
								String registerName = registerNames[x];
								if (registers.get(registerName) == null) {
									System.out.println(String.format("\t<%s>%s<%s>", registerName, "Register does not exist.", registerName));
									continue;
								}
								System.out.print(String.format("\t<%s>%016x</%s>\n", registerName, registers.get(registerName).getValue().longValue(), registerName));
							}
							System.out.println("</registers>");
						} else {
							for (int x = 0; x < registerNames.length; x += perCol) {
								for (int y = 0; y < perCol; y++) {
									if (x + y >= registerNames.length) {
										break;
									}
									String registerName = registerNames[x + y];
									if (registers.get(registerName) == null) {
										System.out.print(String.format("%15s = %16s ", registerName, "Unrecognized reg"));
										continue;
									}
									System.out.print(String.format("%15s = %016x ", registerName, registers.get(registerName).getValue().longValue()));
								}
								System.out.println();
							}
						}

					} else if (command.equals("s") || command.matches("s [0-9]+[kmb]?")) {
						int steps = 1;
						if (command.endsWith("k")) {
							command = command.substring(0, command.length() - 1);
							steps = Integer.parseInt(command.split(" ")[1]) * 1000;
						} else if (command.endsWith("m")) {
							command = command.substring(0, command.length() - 1);
							steps = Integer.parseInt(command.split(" ")[1]) * 1000000;
						} else if (command.endsWith("b")) {
							command = command.substring(0, command.length() - 1);
							steps = Integer.parseInt(command.split(" ")[1]) * 1000000000;
						} else if (command.matches("s [0-9]+")) {
							steps = Integer.parseInt(command.split(" ")[1]);
						}
						for (int i = 0; i < steps; i++) {
							if (bp_flag == 0) {
								executeCPU();
							}
						}
					} else if (command.startsWith("x")) {
						String[] split_text = command.split(" ");
						String addr = null;

						if (split_text.length >= 2) {
							String second_text = split_text[1];
							if (split_text.length > 2) {
								addr = split_text[2];
							}
							if (second_text.charAt(0) == '/') {
								if (second_text.matches("/[0-9]+[bhwg]{1}[xduot]{1}")) {
									x_n = Integer.parseInt(second_text.replaceAll("\\D", ""));
									x_u = getUnitsize(second_text);
									x_f = getPrintformat(second_text);
								} else if (second_text.matches("/[0-9]+")) {
									x_n = Integer.parseInt(second_text.replaceAll("\\D", ""));
								} else if (second_text.matches("/[bhwg]{1}")) {
									x_u = getUnitsize(second_text);
								} else if (second_text.matches("/[xduot]{1}")) {
									x_f = getPrintformat(second_text);
								} else if (second_text.matches("/[0-9]+[bhwg]{1}")) {
									x_n = Integer.parseInt(second_text.replaceAll("\\D", ""));
									x_u = getUnitsize(second_text);
								} else if (second_text.matches("/[0-9]+[xduot]{1}")) {
									x_n = Integer.parseInt(second_text.replaceAll("\\D", ""));
									x_f = getPrintformat(second_text);
								} else if (second_text.matches("/[bhwg]{1}[xduot]{1}")) {
									x_u = getUnitsize(second_text);
									x_f = getPrintformat(second_text);
								} else {
									System.out.println("illegal instruction x  /nuf addr\nn ( Count of how many units to display)\nu - Unit size [b Individual bytes ,h Halfwords (2 bytes) ,w Words (4 bytes) , g Giant words (8 bytes)]\nf - Printing format[ x (Print in hexadecimal) ,d (Print in decimal) ,u (Print in unsigned decimal) ,o (Print in octalt) ,t (Print in binary)]\n");
									continue;
								}
							} else if (second_text.startsWith("0x")) {
								addr = split_text[1];
							} else {
								System.out.println("illegal instruction x  /nuf addr\nn ( Count of how many units to display)\nu - Unit size [b Individual bytes ,h Halfwords (2 bytes) ,w Words (4 bytes) , g Giant words (8 bytes)]\nf - Printing format[ x (Print in hexadecimal) ,d (Print in decimal) ,u (Print in unsigned decimal) ,o (Print in octalt) ,t (Print in binary)]\n");
								continue;
							}
							if (addr != null) {
								handleManipulatingMemory(CommonLib.string2long(addr), x_n, x_u, x_f);
							}
						}
						if (addr == null) {
							handleManipulatingMemory(registers.get("pc").getValue().longValue(), x_n, x_u, x_f);
						}
						System.out.println();
					} else if (command.matches("break 0x[[0-9][a-f][A-F]]+")) {
						String break_memory = command.split(" ")[1].substring(2);
						bp.put(break_memory, 1);
						System.out.println("Set breakpoint successfully");
					} else if (command.equals("info break")) {
						Iterator iterator = bp.entrySet().iterator();
						int count = 0;
						while (iterator.hasNext()) {
							Map.Entry entry = (Map.Entry) iterator.next();
							String key = (String) entry.getKey();
							System.out.println("b" + count + ". 0x" + key);
							count++;
						}
						if (count == 0) {
							System.out.println("There is no breakpoint");
						}
					} else if (command.matches("delete [0-9]+")) {
						Iterator iterator = bp.entrySet().iterator();
						int count = 0;
						int num = Integer.parseInt(command.split(" ")[1]);

						while (iterator.hasNext()) {
							Map.Entry entry = (Map.Entry) iterator.next();
							String key = (String) entry.getKey();
							if (num == count) {
								bp.remove(key);
								System.out.println("Delete breakpoint successfully");
								break;
							}
							count++;
						}
						if (bp.size() == 0) {
							System.out.println("There is no breakpoint");
						}
					} else if (command.startsWith("set")) {
						//Two kinds of usage, set registers or memory
						String[] split_text = command.replaceAll("[ ,]+", " ").split(" ");
						if (split_text.length <= 2) {
							System.out.println("Incorrect format: Try again with \"set <register | memory> [value]\"\n");
							continue;
						}
						if (!Character.isDigit(split_text[1].charAt(0))) { //Set register if second text isnt memory region
							long val = new BigInteger(split_text[2].startsWith("0x") ? split_text[2].substring(2) : split_text[2], 16).longValue();
							registers.get(split_text[1]).setValue(val);
							if (split_text[1].startsWith("pmp")) {
								PMP.updatePmp(split_text[1]);
							}
							System.out.println("register " + split_text[1] + " is set!");
						} else {
							ArrayList<Long> data = new ArrayList<>();
							if (split_text.length >= 3) {
								long addr = (split_text[1].startsWith("0x")) ? new BigInteger(split_text[1].substring(2), 16).longValue() : new BigInteger(split_text[1], 10).longValue();
								for (int i = 2; i < split_text.length; i++) {
									split_text[i] = (split_text[i].startsWith("0x")) ? split_text[i].substring(2) : Long.toHexString(new BigInteger(split_text[i]).longValue());
									if (split_text[i].length() % 2 == 1) {
										split_text[i] = "0" + split_text[i];
									}
									int length = split_text[i].length();
									while (length != 0) {
										data.add(Long.parseLong(split_text[i].substring(length - 2, length), 16));
										length -= 2;
									}
								}
								for (int i = 0; i < data.size(); i++) {
									memory.writeByte(addr + i, (byte) (data.get(i) & 0xff), true, false);
								}
							}
						}
					} else if (command.startsWith("fill")) {
						String[] split_text = command.split(" ");
						long start = Long.parseLong(split_text[1].substring(2), 16);
						long dest = Long.parseLong(split_text[2].substring(2), 16);
						long size = dest - start;
						long value = Long.parseLong(split_text[3].substring(2), 16);
						for (int i = 0; i < size; i++) {
							memory.writeByte(start + i, (byte) (value & 0xff), true, false);
						}
					} else if (command.startsWith("compare")) {
						String[] split_text = command.split(" ");
						long compare_length = 64;
						if (split_text.length > 3) {
							if (split_text[3].startsWith("0x")) {
								compare_length = new BigInteger(split_text[3].substring(2), 16).longValue();
							} else {
								compare_length = new BigInteger(split_text[3], 10).longValue();
							}
						}
						long res1 = memory.readGodMode(CommonLib.string2long(split_text[1]), 8);
						long res2 = memory.readGodMode(CommonLib.string2long(split_text[2]), 8);
						if (compare_length != 64) {
							if ((res1 & ((1L << compare_length) - 1)) == (res2 & ((1L << compare_length) - 1))) {
								System.out.println("equal");
							} else {
								System.out.println("not equal");
							}
						} else {
							if (res1 == res2) {
								System.out.println("equal");
							} else {
								System.out.println("not equal");
							}
						}
					} else if (command.equals("cont") || command.equals("c")) {
						bp_flag = 0;
						System.out.println("Continued. Type \"pause\" or \"p\" anytime to stop.");
						c2 = new Thread(() -> {
							while (bp_flag == 0) {
								try {
									executeCPU();
								} catch (RiscvException ex) {
									Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
								} catch (InterruptException ex) {
									Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
								}
							}
						});
						c2.start();
					} else if (command.equals("pause") || command.equals("p")) {
						bp_flag = 1;
						System.out.println("Paused.");
					} else if (command.matches("(disasm|d)( 0x[0-9a-fA-F]+[,]?)?( [0-9]+)?")) {
						String[] split_text = command.replaceAll("[ ,]+", " ").split(" ");
						RISCVDisassembler riscv = new RISCVDisassembler();
						int n = 1;
						long address = registers.get("pc").getValue().longValue();
						if (split_text.length > 1) {
							if (split_text[1].startsWith("0x")) {
								address = new BigInteger(split_text[1].substring(2), 16).longValue();
								if (split_text.length > 2) {
									n = Integer.parseInt(split_text[2]);
								}
							} else if (split_text[1].matches("\\d+")) {
								n = Integer.parseInt(split_text[1]);
							}
						}
						if (isXml) {
							System.out.println("<disassembler>");
						}
						for (int i = 0; i < n; i++) {
							String out = Long.toHexString(address);
							if (isXml) {
								System.out.print("\t<name>" + "0".repeat(8 - out.length()) + out + "!");
							} else {
								System.out.print("0x" + "0".repeat(8 - out.length()) + out + ": ");
							}

							int noOfByte = RISCVDisassembler.getNoOfByte(memory.read(address, true, false) & 0x7f);
							riscv.setCurrentOffset(address);
							long t;
							int[] ins;
							if (noOfByte == 4) {
								ins = new int[4];
								ins[0] = memory.read(address, true, false) & 0xff;
								ins[1] = memory.read(address + 1, true, false) & 0xff;
								ins[2] = memory.read(address + 2, true, false) & 0xff;
								ins[3] = memory.read(address + 3, true, false) & 0xff;
								t = address + 4;
							} else {
								ins = new int[2];
								ins[0] = memory.read(address, true, false) & 0xff;
								ins[1] = memory.read(address + 1, true, false) & 0xff;
								t = address + 2;
							}
							if (isXml) {
								System.out.print(CommonLib.arrayToHexString(ins) + "!");
							}
							Line line = getLine(address);
							System.out.print(line.code);
							address = t;
							if (isXml) {
								System.out.print("</name_" + "0".repeat(8 - out.length()) + out + ">\n");
							} else {
								System.out.println();
							}
						}
						if (isXml) {
							System.out.println("</disassembler>");
						}
					} else if (command.startsWith("find")) {
						String[] threeSegment = command.replaceAll("[ ,]+", " ").substring(5).split(" ");

						long start = Long.parseLong(threeSegment[0].substring(2), 16);
						long end = Long.parseLong(threeSegment[1].substring(2), 16);
						ArrayList<Long> data = new ArrayList<>();
						ArrayList<String> word = new ArrayList<>();
						ArrayList<Integer> skipping = new ArrayList<>();
						long target = 0;
						boolean skip = false;
						if (isXml) {
							System.out.println("<matchedMemory>");
						}
						for (int j = 2; j < threeSegment.length; j++) {
							String str_target = threeSegment[j];
							//check if there is "?"
							if (str_target.contains("?")) {
								skip = true;

							}
							if (str_target.matches("0x[[0-9][a-f][A-F]]+")) {
								word.add(str_target);
							} else {
								for (int x = 0; x < str_target.length(); x++) {
									char ch = str_target.charAt(x);
									word.add(String.valueOf(ch));
								}
							}
						}
						if (skip) {
							for (int x = 0; x < word.size(); x++) {
								if (word.get(x).equals("?")) {
									skipping.add(1);
								} else {
									skipping.add(0);
								}
							}
						}
						for (int j = 0; j < word.size(); j++) {
							String str_target = word.get(j);
							if (str_target.matches("0x[[0-9][a-f][A-F]]+")) {
								target = Long.parseLong(str_target.substring(2), 16);
								data.add(target);
							} else {
								target = str_target.charAt(0);
								data.add(target);
							}
						}
						for (int i = 0; i < end - start + 1; i++) {
							int j = 0;
							boolean boo = false;
							int address = (int) (start + i);
							if (skip) {
								do {
									if (skipping.get(j) == 1) {

										j++;
										boo = true;
										continue;
									}
									long temp_addrvalue = (memory.read(address + j, true, false) & 0xff);
									long temp_listvalue = data.get(j);
									if (temp_addrvalue == temp_listvalue) {
										boo = true;
									} else {
										boo = false;
									}
									j++;
								} while (j < data.size() && boo == true);
							} else {
								do {
									long temp_addrvalue = (memory.read(address + j, true, false) & 0xff);
									long temp_listvalue = data.get(j);
									if (temp_addrvalue == temp_listvalue) {
										boo = true;
									} else {
										boo = false;
									}
									j++;
								} while (j < data.size() && boo == true);
							}
							if (boo) {
								for (j = 0; j < data.size(); j++) {
									if (isXml) {
										System.out.println(String.format("<address target=\"%d\">%s</address>", (data.get(j)), "0x" + Integer.toHexString(address + j)));
									} else {
										System.out.println("0x" + Integer.toHexString(address + j));
									}
								}
							}
						}
						if (isXml) {
							System.out.println("</matchedMemory>");
						}
					} else if (command.startsWith("paging")) {
						command = command.replaceAll("[ ,]+", " ");
						String[] split_text = command.split(" ");
						long satp = registers.get("satp").getValue().longValue();
						long mode = satp >>> 60, asid = (satp & 0xffff00000000000L) >> 44, physicalPageNumber = satp & 0xfffffffffffL;
						if (command.equals("paging")) {
							System.out.println(String.format("satp: 0x%016x\n  +------+------+----------------------+\n  |63  60|59  44|43                   0|\n  | mode | ASID | physical page number |\n  | 0x%x  | 0x%x  | 0x%011x        |\n  +------+------+----------------------+\n", satp, mode, asid, physicalPageNumber));
							if (satp != 0) { //command "check paging" will return paging mode and status
								System.out.print("paging: on\nmode  : ");
								if (mode == 8) {
									System.out.println("sv39");
								} else if (mode == 9) {
									System.out.println("sv48");
								} else if (mode == 0) {
									System.out.println("No protection or no translation");
								} else {
									System.out.println("Unrecognized");
								}
							} else {
								System.out.println("paging is off\nTurn on paging by \"set satp [value]\". [hint: mode 8 = sv39, mode 9 = sv48, mode 0 = no translation]");
							}
						} else if (command.matches("paging sv39 address (0x)?[0-9a-fA-F]+")) {
							long va_Value;
							if (split_text[3].startsWith("0x")) {
								va_Value = new BigInteger(split_text[3].substring(2), 16).longValue();
							} else {
								va_Value = new BigInteger(split_text[3], 10).longValue();
							}
							long pageOffset = va_Value & 0xFFF, vpn2 = (va_Value >> 30) & 0x1FF, vpn1 = (va_Value >> 21) & 0x1FF, vpn0 = (va_Value >> 12) & 0x1FF;
							System.out.println(String.format("VPN[2] | VPN[1] | VPN[0] | page offset\n0x%03x  | 0x%03x  | 0x%03x  | 0x%03x", vpn2, vpn1, vpn0, pageOffset));
						} else if (command.matches("paging sv39 dump .*")) {
							if (mode == 8) { //must be in sv39 mode to do the following commands
								long start_address = (split_text[3].startsWith("0x")) ? new BigInteger(split_text[3].substring(2), 16).longValue() : physicalPageNumber << 12;
								int start_index = (split_text[3].startsWith("0x")) ? 4 : 3;
								System.out.println(String.format("Page table starting address: 0x%x", start_address));
								for (int i = start_index; i < split_text.length; i++) {
									long line;
									if (split_text[i].matches("[0-9]+") && (line = Long.valueOf(split_text[i])) < 512) {
										long pte = memory.readGodMode(start_address + line * 8, 8);
										long pte_res = (pte >> 54) & 0x3ff, pte_ppn2 = (pte >> 28) & 0x3ffffff, pte_ppn1 = (pte >> 19) & 0x1ff, pte_ppn0 = (pte >> 10) & 0x1ff, pte_rsw = (pte >> 8) & 0x3, pte_d = (pte >> 7) & 1, pte_a = (pte >> 6) & 1, pte_g = (pte >> 5) & 1, pte_u = (pte >> 4) & 1, pte_x = (pte >> 3) & 1, pte_w = (pte >> 2) & 1, pte_r = (pte >> 1) & 1, pte_v = pte & 1;
										if (i == start_index) {
											System.out.println("                                |63      54|53       28|27   19|18   10|9   8|7|6|5|4|3|2|1|0|\n                                | reserved |   PPN2    | PPN1  | PPN0  | RSW |D|A|G|U|X|W|R|V|");
										}
										System.out.println(String.format("entry%3d: 0x%016x -> |  0x%03x   | 0x%07x | 0x%03x | 0x%03x | 0x%x |%d|%d|%d|%d|%d|%d|%d|%d|", Long.valueOf(split_text[i]), pte, pte_res, pte_ppn2, pte_ppn1, pte_ppn0, pte_rsw, pte_d, pte_a, pte_g, pte_u, pte_x, pte_w, pte_r, pte_v));
									} else {
										System.out.println("Error Page entry " + split_text[i]);
									}
								}
							} else {
								System.out.println("Turn on sv39 paging mode to continue. Type \"check paging\" for more.");
							}
						} else if (command.matches("paging sv39 translate (0x)?[0-9a-fA-F]+")) {
							long trans_value;
							if (mode == 8) {
								trans_value = split_text[3].startsWith("0x") ? new BigInteger(split_text[3].substring(2), 16).longValue() : new BigInteger(split_text[3], 16).longValue();
								System.out.println(String.format("VA: %016x\nPA: %016x ", trans_value, memory.getMappedAddress(trans_value, true)));
							} else {
								System.out.println("Turn on sv39 paging mode to continue. Type \"check paging\" for more.");
							}
						} else {
							System.out.println("Unrecognized Command. Try again by:\n"
									+ "\tpaging sv39 address [virtual address]\n"
									+ "\tpaging sv39 dump [entry number(s)]\n"
									+ "\tpaging sv39 dump [starting address] [entry number(s)]\n"
									+ "\tpaging sv39 translate [virtual address]");
						}
					} else if (command.startsWith("pmp")) {
						if (command.equals("pmp")) {
							PMP.printAllPMPSettings();
						} else {
							String[] split_text = command.replaceAll("[ ,]+", " ").split(" ");
							for (int i = 1; i < split_text.length; i++) {
								int index;
								if (split_text[i].matches("\\d+") && (index = Integer.parseInt(split_text[i])) >= 0 && Integer.parseInt(split_text[i]) < 16) {
									PMP.printPMP(index);
								} else {
									System.out.println("Unrecognized register pmpaddr" + split_text[i] + ". Try again with pmp [0-15]...");
								}
							}
						}
					} else if (command.matches("line( (on|off))?")) {
						if (command.equals("line")) {
							printDisassembleLine = !printDisassembleLine;
						} else if (command.split(" ")[1].equals("on")) {
							printDisassembleLine = true;
						} else {
							printDisassembleLine = false;
						}
						System.out.println(printDisassembleLine ? "on" : "off");
					} else if (command.equals("memoryhistory")) {
						if (memory.operands.isEmpty()) {
							System.out.println("memory.operands is empty");
						} else {
							for (MemoryOperand temp : memory.operands) {
								System.out.println(temp);
							}
						}
					} else if (command.equals("time")) {
						System.out.println(new SimpleDateFormat("HH:mm:ss").format(new Date()));
					} else if (command.equals("log")) {
						if (logThread == null) {
							System.out.println("log is off");
						} else {
							System.out.println("log to " + logThread);
						}
					} else if (command.equals("log off")) {
						logThread = null;
						System.out.println("ok");
					} else if (command.equals("cputick")) {
						System.out.println(cpuTick);
					} else if (command.equals("help")) {
						System.out.println("RISC-V Simulator || Commands\n-------------------------------------------------");
						System.out.println("Basic Usage: ");
						commandHelper("q", "Exit Simulator.");
						commandHelper("v", "Version of simulator.");
						commandHelper("s [num]?", "Single step num/1 instruction(s).");
						commandHelper("c / cont", "Continue simulation.");
						commandHelper("p / pause", "Pause simulation.");
						commandHelper("line [on/off]?", "Print out disassemble information during step simulation");
						commandHelper("xml [on/off]?", "Print output in xml format.");
						commandHelper("r [regs]?", "Print register values.");
						commandHelper("r info [csr]", "Print specific information on CSR registers.");
						commandHelper("x /[n]?[u]?[f]? 0x[addr]?", "Print memory according to user specified formats.");
						commandHelperUsage("n    - Number of units to display [integer].");
						commandHelperUsage("u    - Unit size. [b (byte), h (halfword/2 bytes), w (words/4 bytes), g (giant words/8 bytes)]");
						commandHelperUsage("f    - Printing format. [x (hexadecimal), d (decimal), u (unsigned decimal), o (octal), t (binary)]");
						commandHelperUsage("addr - Starting address in hexadecimal or defaulted as pc if not specified.");
						commandHelperUsage("eg: [ x /10bx 0x1000 ] will result in printing 10 bytes in hexadecimal format starting from 0x1000 ");
						commandHelper("set [reg] [val]", "Set value for a register.");
						commandHelper("set [mem] [vals]", "Set values for memory.");
						commandHelperUsage("eg: [ set 0x1000 0x12345678, 0x55, 0x44 ] will result in storing 0x445512345678 in 0x1000");
						commandHelper("fill", "Dont know what it do.");
						commandHelper("compare [A] [B] [len]?", "Compare memory region A and memory region B in terms of len/64 bits.");
						commandHelper("find", "Dont know what it do.");
						commandHelper("paging", "Check status and mode of paging.");
						commandHelper("paging sv39 address [va]", "Get virtual page number and offset of specified Virtual Address at mode sv39.");
						commandHelper("paging sv39 dump [addr]? [entries]", "Get page table entries at given page table starting address at mode sv39.");
						commandHelperUsage("If [addr] isn't specified, starting address will be default to physical address stated at satp register.");
						commandHelper("paging sv39 translate [va]", "Translate Virtual Address to Physical Address at mode sv39.");
						commandHelper("pmp [entries]?", "Print specific/all Physical Memory Protection entries.");
						commandHelper("priv", "Print current Privilege mode.");
						commandHelper("break [addr]", "Set a Breakpoint at specified address");
						commandHelper("info break", "List information of all existing breakpoints");
						commandHelper("delete [bp_num]", "Delete the breakpoint with index of bp_num");
						commandHelper("disasm/d [addr]?, [num]?", "Disassembles num/1 instruction(s) starting from addr/pc.");
						commandHelper("time", "Print current time.");
						commandHelper("log", "Print log db path.");
						commandHelper("log off", "Turn off log to db.");
						commandHelper("cputick", "show current cputick.");
					} else {
						System.out.println("Unrecognized Command.");
					}
				}
			} catch (PageFaultException ex) {
				ex.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		if (dumpJson != null && new File(dumpJson).exists()) {
			FileUtils.writeStringToFile(new File(dumpJson), "]", "utf8", true);
			zip(new File(dumpJson));
		}
		if (logThread != null) {
			logThread.stop();
		}
		System.out.println("quit");
		if (!Startcont) {
			System.exit(0);
		}
	}

	public void commandHelper(String command, String help) {
		System.out.println(String.format("%-37s : %s", command, help));
	}

	public void commandHelperUsage(String help) {
		System.out.println(String.format("%-37s  -> %s", "", help));
	}

//	public long getSpecificBits(long val, int startingBit, int length) {
//		return (length == 64) ? val & 0xffffffffffffffffL : (val >>> startingBit) & ((1L << length) - 1);
//	}
	public static String centerString(int width, String s) {
		return String.format("%-" + width + "s", String.format("%" + (s.length() + (width - s.length()) / 2) + "s", s));
	}

	public void generateString(long value, String bits, String names) {
		String[] bitArray = bits.split("\\|");
		String[] nameArray = names.split("\\|");
		String bitLine = "\t|", nameLine = "\t|", valLine = "\t|";
		for (int i = 0; i < bitArray.length; i++) {
			String bitStr = bitArray[i], name = nameArray[i];
			int s = (bitStr.contains("-")) ? Integer.parseInt(bitStr.split("-")[1]) : Integer.parseInt(bitStr);
			int e = (bitStr.contains("-")) ? Integer.parseInt(bitStr.split("-")[0]) : s;
			if (s > e) {
				int tmp = s;
				s = e;
				e = tmp;
			}
			int length = e - s + 1;
			int bitLength = CommonLib.ceilDiv(length, 4);
			int nameLength = name.length();
			int clamp = Math.max(bitStr.length(), Math.max((bitLength != 1) ? bitLength + 2 : bitLength, (nameLength != 1) ? nameLength + 2 : nameLength));
			if (clamp == 1) {
				bitLine += String.format("%d|", s);
				nameLine += String.format("%s|", name);
				valLine += String.format("%x|", CommonLib.getValue(value, s, s + length - 1));
			} else {
				bitLine += (s != e) ? String.format("%-" + clamp + "s|", String.format("%d %" + (clamp - Integer.toString(e).length() - 1) + "d", e, s)) : String.format("%-" + clamp + "s|", centerString(clamp, Integer.toString(s)));
				nameLine += String.format("%-" + clamp + "s|", centerString(clamp, name));
				valLine += String.format("%-" + clamp + "s|", centerString(clamp, String.format("%0" + bitLength + "x", CommonLib.getValue(value, s, s + length - 1))));
			}
		}
		System.out.println(bitLine + "\n" + nameLine + "\n" + valLine);
	}

	public void printRegisterInfo(String reg) {
		long value = registers.get(reg).getValue().longValue();
		if (reg.equals("mstatus")) {
			generateString(value, "63|62-38|37|36|35-34|33-32|31-23|22|21|20|19|18|17|16-15|14-13|12-11|10-9|8|7|6|5|4|3|2|1|0", "SD|WPRI|MBE|SBE|SXL|UXL|WPRI|TSR|TW|TVM|MXR|SUM|MPRV|XS|FS|MPP|VS|SPP|MPIE|UBE|SPIE|WPRI|MIE|WPRI|SIE|WPRI");
		} else if (reg.equals("misa")) {
			generateString(value, "63-62|61-26|25-0", "MXL|0");
		}
	}

	public void handleManipulatingMemory(long address, int x_n, char x_u, char x_f) {
		String output = null;
		int numBits = x_u == 'b' ? 8 : x_u == 'h' ? 16 : x_u == 'w' ? 32 : x_u == 'g' ? 64 : 0;
		int base = x_f == 'x' ? 16 : x_f == 'd' ? 10 : x_f == 'o' ? 8 : x_f == 't' ? 2 : 0;
		int digits = x_f == 'x' ? numBits / 4 : x_f == 'o' ? (int) Math.ceil(numBits / 3.0) : x_f == 't' ? numBits : (x_f == 'd' || x_f == 'u') ? Long.toString(1L << (numBits - 1)).length() : 0;
		int noPerLine = isXml ? 8 : 32 / (numBits / 8);
		if (isXml) {
			System.out.println("<memory>");
		}
		for (int i = 0; i < x_n; i++) { //For number of times
			if (i % noPerLine == 0) { //Line formatter
				if (i != 0) {
					address = isXml ? address + numBits : address + 32;
				}
				if (isXml) {
					if (i > 0) {
						System.out.println("</address>");
					}
					System.out.print(String.format("<address value=\"0x%08x\">", address));
				} else {
					if (i > 0) {
						System.out.println();
					}
					System.out.print(String.format("0x%08x: ", address));
				}
			}
			long val = 0;
			try { //Get Value from Memory
				val = memory.readGodMode(address + ((i % noPerLine) * numBits / 8), numBits / 8);
			} catch (IRQException | PageFaultException ex) {
				return;
			}
			if (x_f == 't') {
				output = Long.toBinaryString(val);
			} else if (x_f == 'o') {
				output = Long.toOctalString(val);
			} else if (x_f == 'x') {
				output = Long.toHexString(val);
			} else if (x_f == 'u') {
				output = new BigInteger(Long.toHexString(val), 16).toString();
			} else if (x_f == 'd') {
				output = Long.toString(val);
			}
			if (x_f == 'd' || x_f == 'u') {
				System.out.print(" ".repeat(digits - output.length()) + output + " ");
			} else {
				System.out.print("0".repeat(digits - output.length()) + output + " ");
			}
		}
		if (isXml) {
			System.out.println("</address>\n</memory>");
		}
	}

	public char getUnitsize(String str) {
		char c = str.charAt(0);
		for (int i = 0; i < str.length(); i++) {
			c = str.charAt(i);
			if (c == 'b' || c == 'h' || c == 'w' || c == 'g') {
				return c;
			}
		}
		return c;
	}

	public char getPrintformat(String str) {
		char c = str.charAt(0);
		for (int i = 0; i < str.length(); i++) {
			c = str.charAt(i);
			if (c == 'x' || c == 'd' || c == 'u' || c == 'o' || c == 't') {
				return c;
			}
		}
		return c;
	}

	private void modifyPC() throws NoOfByteException, PageFaultException, IRQException {
		long pc = registers.get("pc").getValue().longValue();
		int noOfByte = RISCVDisassembler.getNoOfByte(memory.read(pc, true, false) & 0x7f);

		if (noOfByte == 4) {
			registers.get("pc").setValue(registers.get("pc").getValue().longValue() + 4);
		} else {
			registers.get("pc").setValue(registers.get("pc").getValue().longValue() + 2);
		}
	}

	public void executeCPU() throws RiscvException, InterruptException {
		try {
			long pc = registers.get("pc").getValue().longValue();
//			if (cpuTick == 10680613l) {
//				int fuck = 1234;
//			}
//			if (pc == 0x80010c10l) {
//				System.out.printf("pc=0x%x, cputick=%d\n", pc, cpuTick);
//			}
			handleMemoryHiJack(cpuTick);

			FireInterrupt fireInterrupt = handleInterrupt();
			if (fireInterrupt != null) {
				long address = registers.get("pc").getValue().longValue();
				long value = 0;
				if (CPUState.priv == CPUState.MACHINE_MODE) {
					registers.get("mcause").setValue(BigInteger.valueOf(fireInterrupt.cause).or(new BigInteger("8000000000000000", 16)));
					registers.get("mepc").setValue(address);

					long temp = 1 << registers.get("scause").getValue().longValue();
					if ((registers.get("mideleg").getValue().longValue() & temp) == temp) {
						value = registers.get("stvec").getValue().longValue();
					} else {
						value = registers.get("mtvec").getValue().longValue();
					}
				} else if (CPUState.priv == CPUState.SUPERVISOR_MODE) {
					registers.get("scause").setValue(BigInteger.valueOf(fireInterrupt.cause).or(new BigInteger("8000000000000000", 16)));
					registers.get("sepc").setValue(address);
					value = registers.get("stvec").getValue().longValue();

					BigInteger mstatus = registers.get("mstatus").getValue();
					mstatus = mstatus.or(BigInteger.valueOf(0x120));
					mstatus = mstatus.and(BigInteger.valueOf(0xfffffffffffffffdl));
					registers.get("mstatus").setValue(mstatus);

					BigInteger sstatus = registers.get("sstatus").getValue();
					sstatus = sstatus.or(BigInteger.valueOf(0x120));
					sstatus = sstatus.and(BigInteger.valueOf(0xfffffffffffffffdl));
					registers.get("sstatus").setValue(sstatus);
				} else if (CPUState.priv == CPUState.USER_MODE) {
					value = registers.get("utvec").getValue().longValue();
//				registers.get("ucause").setValue(BigInteger.valueOf(ex.fireInterrupt.cause).or(new BigInteger("8000000000000000", 16)));
					registers.get("uepc").setValue(address);

					BigInteger ustatus = registers.get("ustatus").getValue();
					ustatus = ustatus.or(BigInteger.valueOf(0x120));
					ustatus = ustatus.and(BigInteger.valueOf(0xfffffffffffffffdl));
					registers.get("ustatus").setValue(ustatus);
				} else {
					System.out.println("fuck interrupt");
					System.exit(123);
				}
				registers.get("pc").setValue(value);

//			long mstatus = registers.get("mstatus").value.longValue();
//			if (CPUState.priv == CPUState.SUPERVISOR_MODE) {
//				mstatus = mstatus | 0x120;
//			}
//			registers.get("mstatus").setValue(mstatus);
				registers.get("mip").setValue(registers.get("mip").getValue().longValue() | 0x80);
				CPUState.setPriv(3);

				Setting.getInstance().removeFireInterrupt(cpuTick);
			}

			pc = registers.get("pc").getValue().longValue();

			if (bp.get(Long.toHexString(pc)) != null) {
				if (bp.get(Long.toHexString(pc)) == 1) {
					System.out.println("Breakpoint Hit!");
					bp_flag = 1;
					bp.put(Long.toHexString(pc), 0);
				}
			}
			boolean invalid = false;
			long insn = 0;
			int noOfByte = RISCVDisassembler.getNoOfByte(memory.read(pc, true, false) & 0x7f);

			//dump to json
			if (dumpJson != null) {
				int[] ins;
				if (noOfByte == 4) {
					ins = new int[4];
					ins[0] = memory.read(pc, true, false) & 0xff;
					ins[1] = memory.read(pc + 1, true, false) & 0xff;
					ins[2] = memory.read(pc + 2, true, false) & 0xff;
					ins[3] = memory.read(pc + 3, true, false) & 0xff;
				} else {
					ins = new int[2];
					ins[0] = memory.read(pc, true, false) & 0xff;
					ins[1] = memory.read(pc + 1, true, false) & 0xff;
				}
				dumpToJson(pc, ins);
			}
			if (logThread != null) {
				int[] ins;
				if (noOfByte == 4) {
					ins = new int[4];
					ins[0] = memory.read(pc, true, false) & 0xff;
					ins[1] = memory.read(pc + 1, true, false) & 0xff;
					ins[2] = memory.read(pc + 2, true, false) & 0xff;
					ins[3] = memory.read(pc + 3, true, false) & 0xff;
				} else {
					ins = new int[2];
					ins[0] = memory.read(pc, true, false) & 0xff;
					ins[1] = memory.read(pc + 1, true, false) & 0xff;
				}
				Line line = getLine(pc);
				dumpToLog(ins, line, cpuTick);
			}

			//print disasm line during execution
			if (printDisassembleLine) {
				Line line = getLine(pc);
				if (dwarfArrayList == null) {
					System.out.println(String.format("%5d, 0x%08x, [%-19s]: %s", cpuTick, pc, CommonLib.arrayToHexString(line.bytes), line.code));
				} else {
					ArrayList<String> s = QuantrDwarf.getCCode(dwarfArrayList, BigInteger.valueOf(pc), false);
					if (s != null) {
						for (String temp : s) {
							System.out.println(ConsoleColor.blue + temp);
						}
					}
					System.out.println(String.format("%5d, 0x%08x, [%-19s]: %s", cpuTick, pc, CommonLib.arrayToHexString(line.bytes), line.code));
				}
			}
			if (noOfByte == 4) {
				insn = memory.read(pc, 4, true, false);
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + 4);
				int opcode = (int) insn & 0x7f, rd_num = ((int) insn >> 7) & 0x1f, rs1_num = ((int) insn >> 15) & 0x1f, rs2_num = ((int) insn >> 20) & 0x1f, funct3 = ((int) insn >> 12) & 0x7, funct7 = ((int) insn >> 25) & 0x7f;
				String rd = Registers.getRegXNum32(rd_num), rs1 = Registers.getRegXNum32(rs1_num), rs2 = Registers.getRegXNum32(rs2_num);
				switch (opcode) {
					case 0x3 -> {
						long imm = signExtend64(insn >> 20, 11);
						long addr = registers.get(rs1).getValue().longValue() + imm;

						switch (funct3) {
							case 0 -> { //lb
								registers.get(rd).setValue(memory.read(addr, 1, true, false));
							}
							case 1 -> { //lh
								registers.get(rd).setValue(memory.read(addr, 2, true, false));
							}
							case 2 -> { //lw
								registers.get(rd).setValue(memory.read(addr, 4, true, false));
							}
							case 3 -> { //ld
								registers.get(rd).setValue(memory.read(addr, 8, true, false));
							}
							case 4 -> { //lbu
								registers.get(rd).setValue(memory.read(addr, 1, true, false));
							}
							case 5 -> { //lhu
								registers.get(rd).setValue(memory.read(addr, 2, true, false));
							}
							case 6 -> { //lwu
								registers.get(rd).setValue(memory.read(addr, 4, true, false));
							}
							default -> {
								invalid = true;
							}
						}

					}
					case 0xf -> {
						if (funct3 == 0) { //fence

						} else {
							invalid = true;
						}
					}
					case 0x13 -> {
						long imm = signExtend64(insn >> 20, 11), shamt = imm & 0x3f;
						switch (funct3) {
							case 0 -> { //addi
								registers.get(rd).setValue(registers.get(rs1).getValue().longValue() + imm);
							}
							case 1 -> { //slli
								registers.get(rd).setValue(registers.get(rs1).getValue().longValue() << shamt);
							}
							case 2 -> { //slti
								registers.get(rd).setValue((registers.get(rs1).getValue().longValue() < imm) ? 1 : 0);
							}
							case 3 -> { //sltiu
								registers.get(rd).setValue(Long.compareUnsigned(registers.get(rs1).getValue().longValue(), imm) < 0 ? 1 : 0);
							}
							case 4 -> { //xori
								registers.get(rd).setValue(registers.get(rs1).getValue().longValue() ^ imm);
							}
							case 5 -> {
								switch (funct7 >> 1) {
									case 0 -> { //srli
										registers.get(rd).setValue(registers.get(rs1).getValue().longValue() >>> shamt);
									}
									case 0x10 -> { //srai
										registers.get(rd).setValue(registers.get(rs1).getValue().longValue() >> shamt);
									}
									default -> {
										invalid = true;
									}
								}
							}
							case 6 -> { //ori
								registers.get(rd).setValue(registers.get(rs1).getValue().longValue() | imm);
							}
							case 7 -> { //andi
								registers.get(rd).setValue(registers.get(rs1).getValue().longValue() & imm);
							}
							default -> {
								invalid = true;
							}
						}
					}
					case 0x17 -> { //auipc
						long imm = signExtend64(insn & 0xfffff000, 31);
						registers.get(rd).setValue(registers.get("pc").getValue().longValue() + imm - 4);
					}
					case 0x1b -> {
						long imm = signExtend64(insn >> 20, 11), shamt = imm & 0x1f;
						switch (funct3) {
							case 0 -> { //addiw
								registers.get(rd).setValue(signExtend64((registers.get(rs1).getValue().longValue() + imm) & 0xffffffffL, 31));
							}
							case 1 -> { //slliw
								if ((imm & 0x20) == 0) {
									registers.get(rd).setValue(signExtend64((registers.get(rs1).getValue().longValue() << shamt) & 0xffffffffL, 31));
								} else {
									invalid = true;
								}
							}
							case 5 -> {
								if ((imm & 0x20) == 0) {
									switch (funct7) {
										case 0 -> { //srliw
											registers.get(rd).setValue(signExtend64((int) (registers.get(rs1).getValue().longValue()) >>> shamt, 31));
										}
										case 0x20 -> { //sraiw
											registers.get(rd).setValue(signExtend64((int) (registers.get(rs1).getValue().longValue()) >> shamt, 31));
										}
										default -> {
											invalid = true;
										}
									}
								} else {
									invalid = true;
								}
							}
							default -> {
								invalid = true;
							}
						}
					}
					case 0x23 -> {
						long imm = signExtend64(((insn >> 20) & 0xfe0) | ((insn >> 7) & 0x1f), 31), addr = registers.get(rs1).getValue().longValue() + imm;
						switch (funct3) {
							case 0 -> { //sb
								memory.write(addr, registers.get(rs2).getValue().longValue(), 1, false, true);
							}
							case 1 -> { //sh
								memory.write(addr, registers.get(rs2).getValue().longValue(), 2, false, true);
							}
							case 2 -> { //sw
								memory.write(addr, registers.get(rs2).getValue().longValue(), 4, false, true);
							}
							case 3 -> { //sd
								memory.write(addr, registers.get(rs2).getValue().longValue(), 8, false, true);
							}
							default -> {
								invalid = true;
							}
						}
					}
					case 0x2f -> {
						int funct5 = (funct7 >> 2) & 0x1f;
						switch (funct3) {
							case 0x2 -> {
								switch (funct5) {
									case 0 -> { //amoadd.w
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 4, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue() + rs1Value, 4, false, true);
										}
									}
									case 1 -> { //amoswap.w
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 4, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
										}
										memory.write(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue(), 4, false, true);
									}
									case 4 -> { //amoxor.w
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 4, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue() ^ rs1Value, 4, false, true);
										}
									}
									case 8 -> { //amoor.w
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 4, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue() | rs1Value, 4, false, true);
										}
									}
									case 0xc -> { //amoand.w
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 4, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue() & rs1Value, 4, false, true);
										}
									}
									case 0x10 -> { //amomin.w
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 4, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue(), 4, false, true);
										}
									}
									case 0x14 -> { //amomax.w
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 4, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue(), 4, false, true);
										}
									}
									case 0x18 -> { //amominu.w
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 4, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue(), 4, false, true);
										}
									}
									case 0x1c -> { //amomaxu.w
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 4, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue(), 4, false, true);
										}
									}
									default -> {
										invalid = true;
									}
								}
							}
							case 0x3 -> {
								switch (funct5) {
									case 0 -> { //amoadd.d
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 8, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue() + rs1Value, 8, false, true);
										}
									}
									case 1 -> { //amoswap.d
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 8, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue(), 8, false, true);
										}
									}
									case 4 -> { //amoxor.d
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 8, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue() ^ rs1Value, 8, false, true);
										}
									}
									case 8 -> { //amoor.d
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 8, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue() | rs1Value, 8, false, true);
										}
									}
									case 0xc -> { //amoand.d
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 8, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue() & rs1Value, 8, false, true);
										}
									}
									case 0x10 -> { //amomin.d
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 8, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue(), 8, false, true);
										}
									}
									case 0x14 -> { //amomax.d
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 8, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue(), 8, false, true);
										}
									}
									case 0x18 -> { //amominu.d
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 8, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue(), 8, false, true);
										}
									}
									case 0x1c -> { //amomaxu.d
										long rs1Value = memory.read(registers.get(rs1).getValue().longValue(), 8, true, false);
										if (!rd.equals("zero")) {
											registers.get(rd).setValue(rs1Value);
											memory.write(registers.get(rs1).getValue().longValue(), (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue(), 8, false, true);
										}
									}
									default -> {
										invalid = true;
									}
								}
							}
							default -> {
								invalid = true;
							}
						}
					}
					case 0x33 -> {
						long shamt = registers.get(rs2).getValue().longValue() & 0x3f;
						switch (funct7) {
							case 0 -> {
								switch (funct3) {
									case 0 -> { //add
										registers.get(rd).setValue(registers.get(rs1).getValue().longValue() + registers.get(rs2).getValue().longValue());
									}
									case 1 -> { //sll
										registers.get(rd).setValue(registers.get(rs1).getValue().longValue() << shamt);
									}
									case 2 -> { //slt
										registers.get(rd).setValue(registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue() ? 1 : 0);
									}
									case 3 -> { //sltu
										registers.get(rd).setValue(Long.compareUnsigned(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue()));
									}
									case 4 -> { //xor
										registers.get(rd).setValue(registers.get(rs1).getValue().longValue() ^ registers.get(rs2).getValue().longValue());
									}
									case 5 -> { //srl
										registers.get(rd).setValue(registers.get(rs1).getValue().longValue() >>> shamt);
									}
									case 6 -> { //or
										registers.get(rd).setValue(registers.get(rs1).getValue().longValue() | registers.get(rs2).getValue().longValue());
									}
									case 7 -> { //and
										registers.get(rd).setValue(registers.get(rs1).getValue().longValue() & registers.get(rs2).getValue().longValue());
									}
									default ->
										invalid = true;
								}
							}
							case 1 -> {
								switch (funct3) {
									case 0 -> { //mul
										registers.get(rd).setValue(registers.get(rs1).getValue().longValue() * registers.get(rs2).getValue().longValue());
									}
									case 1 -> { //mulh
										registers.get(rd).setValue(mulh(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue()));
									}
									case 2 -> { //mulhsu
										registers.get(rd).setValue(mulhsu(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue()));
									}
									case 3 -> { //mulhu
										registers.get(rd).setValue(mulhu(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue()));
									}
									case 4 -> { //div
										long dividend = registers.get(rs1).getValue().longValue(), divisor = registers.get(rs2).getValue().longValue();
										if (divisor == 0) {
											registers.get(rd).setValue(-1);
										} else if (dividend == 0x8000000000000000L && divisor == -1) {
											registers.get("rd").setValue(dividend);
										} else {
											registers.get(rd).setValue(dividend / divisor);
										}
									}
									case 5 -> { //divu
										registers.get(rd).setValue(registers.get(rs2).getValue().longValue() == 0 ? 0x8000000000000000L : Long.divideUnsigned(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue()));
									}
									case 6 -> { //rem
										long dividend = registers.get(rs1).getValue().longValue(), divisor = registers.get(rs2).getValue().longValue();
										if (divisor == 0) {
											registers.get(rd).setValue(dividend);
										} else if (dividend == 0x8000000000000000L && divisor == -1) {
											registers.get(rd).setValue(0);
										} else {
											registers.get(rd).setValue(registers.get(rs1).getValue().longValue() % registers.get(rs2).getValue().longValue());
										}
									}
									case 7 -> { //remu
										registers.get(rd).setValue(registers.get(rs2).getValue().longValue() == 0 ? registers.get(rs1).getValue().longValue() : Long.remainderUnsigned(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue()));
									}
									default ->
										invalid = true;
								}
							}
							case 0x20 -> {
								switch (funct3) {
									case 0 -> { //sub
										registers.get(rd).setValue(registers.get(rs1).getValue().longValue() - registers.get(rs2).getValue().longValue());
									}
									case 5 -> { //sra
										registers.get(rd).setValue(registers.get(rs1).getValue().longValue() >> shamt);
									}
									default ->
										invalid = true;
								}
							}
							default ->
								invalid = true;
						}
					}
					case 0x37 -> { //lui
						registers.get(rd).setValue(signExtend64(insn & 0xfffff000L, 31));
					}
					case 0x3b -> {
						int shamt = registers.get(rs2).getValue().intValue() & 0x1f;
						switch (funct3) {
							case 0 -> {
								switch (funct7) {//addw
									case 0 -> {
										registers.get(rd).setValue(signExtend64((registers.get(rs1).getValue().intValue() + registers.get(rs2).getValue().intValue()) & 0xffffffff, 31));
									}
									case 1 -> { //mulw
										registers.get(rd).setValue(signExtend64((registers.get(rs1).getValue().intValue() * registers.get(rs2).getValue().intValue()) & 0xffffffff, 31));
									}
									case 0x20 -> { //subw
										registers.get(rd).setValue(signExtend64((registers.get(rs1).getValue().intValue() - registers.get(rs2).getValue().intValue()) & 0xffffffff, 31));
									}
								}
							}
							case 1 -> { //sllw
								registers.get(rd).setValue(signExtend64((registers.get(rs1).getValue().intValue() << shamt) & 0xffffffff, 31));
							}
							case 0x4 -> { //divw 
								long dividend = registers.get(rs1).getValue().intValue(), divisor = registers.get(rs2).getValue().intValue();
								if (divisor == 0) {
									registers.get(rd).setValue(-1);
								} else if (dividend == 0xffffffff80000000L && divisor == -1) {
									registers.get(rd).setValue(dividend);
								} else {
									registers.get(rd).setValue(signExtend64(dividend / divisor, 31));
								}
							}
							case 0x5 -> {
								switch (funct7) {
									case 0 -> {  //srlw
										registers.get(rd).setValue(signExtend64(registers.get(rs1).getValue().intValue() >>> shamt, 31));
									}
									case 1 -> { //divuw
										long dividend = registers.get(rs1).getValue().longValue() & 0xffffffff, divisor = registers.get(rs2).getValue().longValue() & 0xffffffff;
										registers.get(rd).setValue((divisor == 0) ? 0xffffffff80000000L : signExtend64(dividend / divisor, 31));
									}
									case 0x20 -> { //sraw
										registers.get(rd).setValue(signExtend64(registers.get(rs1).getValue().intValue() >> shamt, 31));
									}
									default ->
										invalid = true;
								}
							}
							case 6 -> { //remw
								long dividend = registers.get(rs1).getValue().intValue(), divisor = registers.get(rs2).getValue().intValue();
								if (divisor == 0) {
									registers.get(rd).setValue(dividend);
								} else if (dividend == 0xffffffff80000000L && divisor == -1) {
									registers.get(rd).setValue(0);
								} else {
									registers.get(rd).setValue(signExtend64(dividend % divisor, 31));
								}
							}
							case 7 -> { //remuw
								long dividend = registers.get(rs1).getValue().longValue() & 0xffffffff, divisor = registers.get(rs2).getValue().longValue() & 0xffffffff;
								registers.get(rd).setValue((divisor == 0) ? dividend : signExtend64(dividend % divisor, 31));
							}
							default ->
								invalid = true;
						}

					}
					case 0x63 -> {
						long imm = signExtend64(((int) (insn & 0x80000000) >> 19) | ((insn & 0x80) << 4) | ((insn >> 20) & 0x7e0) | ((insn >> 7) & 0x1e), 12);
						switch (funct3) {
							case 0 -> { //beq
								if (registers.get(rs1).getValue().longValue() == registers.get(rs2).getValue().longValue()) {
									registers.get("pc").setValue(registers.get("pc").getValue().longValue() + imm - 4);
								}
							}
							case 1 -> { //bne
								if (registers.get(rs1).getValue() != registers.get(rs2).getValue()) {
									registers.get("pc").setValue(registers.get("pc").getValue().longValue() + imm - 4);
								}
							}
							case 4 -> { //blt
								if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
									registers.get("pc").setValue(registers.get("pc").getValue().longValue() + imm - 4);
								}
							}
							case 5 -> { // bge
								if (registers.get(rs1).getValue().longValue() >= registers.get(rs2).getValue().longValue()) {
									registers.get("pc").setValue(registers.get("pc").getValue().longValue() + imm - 4);
								}
							}
							case 6 -> { //bltu
								if (Long.compareUnsigned(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue()) < 0) {
									registers.get("pc").setValue(registers.get("pc").getValue().longValue() + imm - 4);
								}
							}
							case 7 -> { //bgeu
								if (Long.compareUnsigned(registers.get(rs1).getValue().longValue(), registers.get(rs2).getValue().longValue()) >= 0) {
									registers.get("pc").setValue(registers.get("pc").getValue().longValue() + imm - 4);
								}
							}
							default -> {
								invalid = true;
							}
						}
					}
					case 0x67 -> { //jalr
						long t = registers.get("pc").getValue().longValue();
						long imm = signExtend64((int) (insn & 0xfff00000) >> 20, 11);
						registers.get("pc").setValue((registers.get(rs1).getValue().longValue() + imm) & ~1);
						registers.get(rd).setValue(t);

					}
					case 0x6f -> {//jal
						registers.get(rd).setValue(registers.get("pc").getValue().longValue());
						long imm = signExtend64(((int) (insn & 0x80000000) >> 11) | (insn & 0xff000) | ((insn >> 9) & 0x800) | ((insn >> 20) & 0x7fe), 20);
						registers.get("pc").setValue(registers.get("pc").getValue().longValue() + imm - 4);
					}
					case 0x73 -> {
						String csr = Registers.getCsrNum12(((int) insn & 0xfff00000) >>> 20);
						switch (funct3) {
							case 0 -> {
								switch (funct7) {
									case 0 -> {
										switch ((int) registers.get(rs2).getValue().longValue()) {
											case 0 -> { //ecall
												switch (CPUState.priv) { //handling the ecall_from {U,S,M} mode
													case 0: //user mode
													case 1: // supervisor_mode
													case 2: // hypervisor_mode
													case 3: //machine mode
														throw new RiscvException(8 + CPUState.priv, "Ecall");
												}
											}
											case 1 -> { //ebreak 
												throw new RiscvException(RiscvException.BREAKPOINT, "Breakpoint");
											}
										}
									}
									case 0x8 -> { //sret
										registers.get("pc").setValue(registers.get("sepc").getValue().longValue());
										switch ((int) (registers.get("sstatus").getValue().longValue() >> 8)) {
											case 0 -> {
												CPUState.priv = CPUState.USER_MODE;
											}
											case 1 -> {
												CPUState.priv = CPUState.SUPERVISOR_MODE;
											}
										}
										switch ((int) registers.get("sstatus").getValue().longValue() >> 5) {
											case 0 -> {
												registers.get("sstatus").setValue((registers.get("sstatus").getValue().longValue()) & (~1 << 1));
											}
											case 1 -> {
												registers.get("sstatus").setValue((registers.get("sstatus").getValue().longValue()) | (1 << 1));
											}
										}
										registers.get("sstatus").setValue((registers.get("sstatus").getValue().longValue()) | (1 << 5));
										registers.get("sstatus").setValue((registers.get("sstatus").getValue().longValue()) & ~(1 << 8));
									}
									case 0x9 -> { //sfence.vma	
									}
									case 0x18 -> { //mret
										registers.get("pc").setValue(registers.get("mepc").getValue().longValue());
										long mpp = ((registers.get("mstatus").getValue().longValue() >> 11) & 3);
										switch ((int) mpp) {
											case 0 -> {
												CPUState.priv = CPUState.USER_MODE;
											}
											case 1 -> {
												CPUState.priv = CPUState.SUPERVISOR_MODE;
											}
											case 2 -> {
												CPUState.priv = CPUState.HYPERVISOR_MODE;
											}
											case 3 -> {
												CPUState.priv = CPUState.MACHINE_MODE;
											}
										}
										switch ((int) (registers.get("mstatus").getValue().longValue() >> 7) & 1) {
											case 0 -> {
												registers.get("mstatus").setValue((registers.get("mstatus").getValue().longValue()) & ~(1 << 3));
											}
											case 1 -> {
												registers.get("mstatus").setValue((registers.get("mstatus").getValue().longValue()) | (1 << 3));
											}
										}
										registers.get("mstatus").setValue((registers.get("mstatus").getValue().longValue()) | (1 << 7));
										registers.get("mstatus").setValue((registers.get("mstatus").getValue().longValue()) & ~(3 << 11));
									}
									default ->
										invalid = true;
								}
							}
							case 0x1 -> { //csrrw 
								long t = registers.get(csr).getValue().longValue();
								registers.get(csr).setValue((registers.get(rs1).getValue().longValue()));
								registers.get(rd).setValue(t);
								//paging_update

							}
							case 0x2 -> { //csrrs
								long t = registers.get(csr).getValue().longValue();
								registers.get(csr).setValue((registers.get(rs1).getValue().longValue()) | t);
								registers.get(rd).setValue(t);
								//paging_update
							}
							case 0x3 -> { //csrrc
								long t = registers.get(csr).getValue().longValue();
								registers.get(csr).setValue((~registers.get(rs1).getValue().longValue()) & t);
								registers.get(rd).setValue(t);
								//paging_update
							}
							case 0x5 -> { //csrrwi
								registers.get(rd).setValue(registers.get(csr).getValue().longValue());
								registers.get(csr).setValue((registers.get(rs1).getValue().longValue()));
								//paging_update
							}
							case 0x6 -> { //csrrsi
								long t = registers.get(csr).getValue().longValue();
								registers.get(csr).setValue((registers.get(rs1).getValue().longValue()) | t);
								registers.get(rd).setValue(t);
								//paging_update
							}
							case 0x7 -> { //csrrci
								long t = registers.get(csr).getValue().longValue();
								registers.get(csr).setValue((~registers.get(rs1).getValue().longValue()) & t);
								registers.get(rd).setValue(t);
							}
							default -> {
								invalid = true;
							}
						}
					}

				}
			} else if (noOfByte == 2) {
				insn = memory.read(pc, 2, true, false);
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + 2);
				int opcode = (int) insn & 0x3, funct3 = (((int) insn >>> 13) & 0x7);
				switch (opcode) {
					case 0 -> { //C0 case
						int rd_num = ((int) insn >> 2) & 0x7, rs1_num = ((int) insn >> 7) & 0x7;
						String rd = Registers.getRegXNum16(rd_num), rs1 = Registers.getRegXNum16(rs1_num);
						switch (funct3) {
							case 0 -> {
								if (rd_num == 0) {
									throw new WrongInstructionException("illegal instruction " + insn);
								} else { //c.addi4spn
									long imm = signExtend64((long) ((insn & 0x400) >>> 11) | ((insn & 0x200) >>> 10) | ((insn & 0x100) >>> 9) | ((insn & 0x80) >>> 8) | ((insn & 0x1000) >>> 7) | ((insn & 0x800) >>> 6) | ((insn & 0x20) >>> 3) | ((insn & 0x40) >>> 2), 9);
									registers.get(rd).setValue(registers.get("sp").getValue().longValue() + imm);
								}
							}
							case 1 -> { //c.fld only as c.lq is for RV128
								long imm = (((insn & 0xc0) >>> 6) | ((insn & 0x1C00) >>> 10) << 8);
								rs1 = Registers.getRegFNum16(rs1_num);
								registers.get(rd).setValue(registers.get(rs1).getValue().longValue() + imm);
							}
							case 2 -> { //c.lw
								long imm = (((insn & 0x4) >>> 2) | ((long) (insn & 0x1C00) >>> 10) | ((insn & 0x40) >>> 5)) << 8;
								registers.get(rd).setValue(registers.get(rs1).getValue().longValue() + imm);
							}
							case 3 -> { // c.ld, c.flw for RV32
								long imm = (((insn & 0xc0) >>> 6) | ((insn & 0x1C00) >>> 10) << 8);
								registers.get(rd).setValue(registers.get(rs1).getValue().longValue() + imm);
							}
							case 5 -> {// c.fsd only c.sq is for RV128
								long imm = (((insn & 0xc0) >>> 6) | ((insn & 0x1C00) >>> 10) << 8);
								rs1 = Registers.getRegFNum16(rs1_num);
								registers.get(rd).setValue(registers.get(rs1).getValue().longValue() + imm);
							}
							case 6 -> {// c.sw
								long imm = (((insn & 0x4) >>> 2) | ((insn & 0x1C00) >>> 10) | ((insn & 0x40) >>> 5)) << 8;
								registers.get(rd).setValue(registers.get(rs1).getValue().longValue() + imm);
							}
							case 7 -> {//c.sd for both RV64and 128, c.fws for RV32
								long imm = (((insn & 0xc0) >>> 6) | ((insn & 0x1C00) >>> 10) << 8);
								registers.get(rd).setValue(registers.get(rs1).getValue().longValue() + imm);
							}
							default -> {
								invalid = true;
							}
						}
					}
					case 1 -> { //C1 case
						switch (funct3) {
							case 0 -> { //c.nop and c.addi
								int rd_num = ((int) insn >> 6) & 0x1f;
								String rd = Registers.getRegXNum32(rd_num);
								if (rd_num == 0) { //c.nop
									registers.get(rd).setValue(registers.get(rd).getValue().longValue() + 0);
								} else { //c.addi
									long imm = signExtend64(((insn & 0x1000) >> 11) | ((insn & 0x7C) >> 2), 5);
									registers.get(rd).setValue(registers.get(rd).getValue().longValue() + imm);
								}
							}
							case 1 -> { // c.addiw, c.jal for rv32
								int rd_num = ((int) insn >> 6) & 0x1f;
								String rd = Registers.getRegXNum32(rd_num);
								if (rd_num != 0) {
									long imm = signExtend64(((insn & 0x1000) >> 11) | ((insn & 0x7C) >> 2), 5);
									registers.get(rd).setValue(registers.get(rd).getValue().longValue() + imm);
								}
							}
							case 2 -> { //c.li
								int rd_num = ((int) insn >> 6) & 0x1f;
								String rd = Registers.getRegXNum32(rd_num);
								if (rd_num != 0) {
									long imm = signExtend64(((insn & 0x1000) >> 11) | ((insn & 0x7C) >> 2), 5);
									registers.get(rd).setValue(registers.get(rd).getValue().longValue() + imm);
								}
							}

							case 3 -> { //c.addi16sp , c.lui
								int rd_num = ((int) insn >> 6) & 0x1f;
								String rd = Registers.getRegXNum32(rd_num);
								if (rd_num == 2) { //c.addi16sp
									long imm = signExtend64((long) ((insn & 0x200) >>> 11) | ((insn & 0x10) >>> 6) | ((insn & 0x40) >>> 5) | ((insn & 0x180) >>> 3) | ((insn & 0x20) >>> 2), 5);
									registers.get(rd).setValue(registers.get("sp").getValue().longValue() + imm);
								} else if (rd_num != 0) { //c.lui
									long imm = signExtend64(((insn & 0x1000) << 5) | ((insn & 0x7c) << 10), 17);
									if (imm != 0) {
										registers.get(rd).setValue(imm);
									} else {
										invalid = true;
									}
								} else {
									invalid = true;
								}
							}
							case 4 -> { //c.sub,c.xor,c.or,c.and,c.subw,c.addw
								int rd_num = (int) (insn >> 7) & 0x7;
								String rd = Registers.getRegXNum16(rd_num);
								int funct2 = (int) (insn & 0x1800) >>> 11;
								switch (funct2) {
									case 0 -> { //c.srli
										long shamt = ((insn & 0x1000) >> 7) | ((insn & 0x7c) >> 2);
										long value = registers.get(rd).getValue().longValue() >>> shamt;
										registers.get(rd).setValue(value);
									}
									case 1 -> { //c.srai
										long shamt = ((insn & 0x1000) >> 7) | ((insn & 0x7c) >> 2);
										long value = registers.get(rd).getValue().longValue() >> shamt;
										registers.get(rd).setValue(value);
									}
									case 2 -> { //c.andi
										long imm = signExtend64(((insn & 0x1000) >> 7) | ((insn & 0x7c) >> 2), 5);
										registers.get(rd).setValue(registers.get(rd).getValue().longValue() & imm);
									}
									case 3 -> {
										int bit12 = (int) (insn & 0x1000) >> 12, bit5_6 = (int) (insn & 0x60) >> 5, rs2_num = (int) (insn >> 2) & 0x7;
										String rs2 = Registers.getRegXNum16(rs2_num);
										if (bit12 == 0) {
											switch (bit5_6) {
												case 0 -> { //c.sub
													registers.get(rd).setValue(registers.get(rd).getValue().longValue() - registers.get(rs2).getValue().longValue());
												}
												case 1 -> { //c.xor
													registers.get(rd).setValue(registers.get(rd).getValue().longValue() ^ registers.get(rs2).getValue().longValue());
												}
												case 2 -> { //c.or
													registers.get(rd).setValue(registers.get(rd).getValue().longValue() | registers.get(rs2).getValue().longValue());
												}
												case 3 -> { //c.and
													registers.get(rd).setValue(registers.get(rd).getValue().longValue() & registers.get(rs2).getValue().longValue());
												}
												default ->
													invalid = true;
											}
										} else if (bit12 == 1) {
											rs2_num = (int) (insn & 0x1c) >> 2;
											rs2 = Registers.getRegXNum16(rs2_num);
											rd_num = (int) (insn & 0x380) >> 7;
											rd = Registers.getRegXNum16(rd_num);
											switch (bit5_6) {
												case 0 -> { //c.subw
													long val = signExtend64(registers.get(rd).getValue().longValue() - registers.get(rs2).getValue().longValue(), 31);
													registers.get(rd).setValue(val);
												}
												case 1 -> { //c.addw
													long val = signExtend64(registers.get(rd).getValue().longValue() + registers.get(rs2).getValue().longValue(), 31);
													registers.get(rd).setValue(val);
												}
												default ->
													invalid = true;
											}
										}
									}
								}
							}
							case 5 -> { //c.j
								long imm = signExtend64(((insn & 0x1000) >> 1) | ((insn & 0x100) << 2) | ((insn & 0x600) >> 1) | ((insn & 0x40) << 1) | ((insn & 0x80) >> 1) | ((insn & 0x4) << 3) | ((insn & 0x800) >> 7) | ((insn >> 2) & 0xe), 11);
								registers.get("pc").setValue(registers.get("pc").getValue().longValue() + imm - 2);
							}
							case 6 -> { //c.beqz
								int rs1_val = (int) (insn >> 7) & 0x7;
								String rs1 = Registers.getRegXNum16(rs1_val);
								if (registers.get(rs1).getValue().longValue() == 0) {
									long imm = signExtend64(((insn & 0x1000) >> 4) | ((insn & 0x60) << 1) | ((insn & 0x4) << 3) | ((insn & 0xc00) >> 7) | ((insn & 0x18) >> 2), 8);
									registers.get("pc").setValue(registers.get("pc").getValue().longValue() + imm);
								}
							}
							case 7 -> { //c.bnez
								int rs1_val = (int) (insn >> 7) & 0x7;
								String rs1 = Registers.getRegXNum16(rs1_val);
								if (registers.get(rs1).getValue().longValue() != 0) {
									long imm = signExtend64(((insn & 0x1000) >> 4) | ((insn & 0x60) << 1) | ((insn & 0x4) << 3) | ((insn & 0xc00) >> 7) | ((insn & 0x18) >> 2), 8);
									registers.get("pc").setValue(registers.get("pc").getValue().longValue() + imm);
								}
							}
							default ->
								invalid = true;
						}

					}

					case 2 -> {
						int rd_num = (int) (insn & 0xf80) >> 7;
						String rd = Registers.getRegXNum16(rd_num);
						switch (funct3) {
							case 0 -> { //c.slli ,c.slli64
								long imm = (insn & 0x1000) >> 12 | (insn & 0x7C) >> 2;
								registers.get(rd).setValue(registers.get("x2").getValue().longValue() + imm);
							}

							case 1 -> { //c.fldsp 
								long imm = ((insn & 0x1c) >> 2 | (insn & 0x1000) >> 12 | (insn & 0x60) >> 5) << 4;
								registers.get(rd).setValue(registers.get("x2").getValue().longValue() + imm);
							}

							case 2 -> { //c.lwsp
								if (registers.get(rd).getValue().longValue() != 0) {
									long imm = ((insn & 0xc) >> 2 | (insn & 0x1000) >> 12 | (insn & 0x70) >> 4) << 2;
									registers.get(rd).setValue(registers.get("x2").getValue().longValue() + imm);
								}
							}

							case 3 -> {//c.ldsp
								if (registers.get(rd).getValue().longValue() != 0) {
									long imm = ((insn & 0x1c) >> 2 | (insn & 0x1000) >> 12 | (insn & 0x60) >> 5) << 4;
									registers.get(rd).setValue(registers.get("x2").getValue().longValue() + imm);
								} else { //c.flwsp
									long imm = ((insn & 0xc) >> 2 | (insn & 0x1000) >> 12 | (insn & 0x70) >> 4) << 4;
									registers.get(rd).setValue(registers.get("x2").getValue().longValue() + imm);
								}

							}
							case 4 -> { //c.jr , c.mv , c.ebreak, c.jalr, c.add
								int rs1_num = (int) (insn & 0xf80) >> 7, rs2_num = (int) (insn & 0x7C) >> 2;
								String rs1 = Registers.getRegXNum16(rs1_num), rs2 = Registers.getRegXNum16(rs2_num);
								if (registers.get(rs1).getValue().longValue() != 0) { //c.jr
									registers.get(rd).setValue(registers.get(rs1).getValue().longValue());
								} else if (registers.get(rd).getValue().longValue() != 0 && registers.get(rs2).getValue().longValue() != 0) { //c.mv
									registers.get(rd).setValue(registers.get(rs2).getValue().longValue());
								} else if (rd_num == 0) {
									throw new RiscvException(RiscvException.BREAKPOINT, "Breakpoint");
								} else if (registers.get(rs1).getValue().longValue() != 0) {
									long t = registers.get("pc").getValue().longValue() + 2;
									registers.get("pc").setValue((registers.get(rs1).getValue().longValue()));
									registers.get("x1").setValue(t);
								} else if ((registers.get(rd).getValue().longValue() != 0 || registers.get(rs1).getValue().longValue() != 0) && registers.get(rs2).getValue().longValue() != 0) {
									registers.get(rd).setValue(registers.get(rd).getValue().longValue() + registers.get(rs2).getValue().longValue());
								}
							}
							case 5 -> {// c.fsdsp
								int rs2_num = (int) (insn & 0x7C) >> 2;
								String rs2 = Registers.getRegXNum16(rs2_num);
								long uimm = ((insn & 0x380) >> 7 | (insn & 0x1c00) >> 10) << 3;
								registers.get(rs2).setValue(registers.get("x2").getValue().longValue() + uimm);
							}
							case 6 -> {//c.swsp
								int rs2_num = (int) (insn & 0x7C) >> 2;
								String rs2 = Registers.getRegXNum16(rs2_num);
								long uimm = ((insn & 0x180) >> 7 | (insn & 0x1e00) >> 9) << 2;
								registers.get(rs2).setValue(registers.get("x2").getValue().longValue() + uimm);
							}
							case 7 -> {//c.sdsp , c.fswsp
								int rs2_num = (int) (insn & 0x7C) >> 2;
								String rs2 = Registers.getRegXNum16(rs2_num);
								long uimm = ((insn & 0x380) >> 7 | (insn & 0x1c00) >> 10) << 3;
								registers.get(rs2).setValue(registers.get("x2").getValue().longValue() + uimm);
							}
							default ->
								invalid = true;
						}
					}
					default -> {
						invalid = true;
					}
				}
			}
			registers.get("x0").setValue(0);
			if (invalid) {
				throw new WrongInstructionException("invalid instruction " + insn);
			}
//			mtime += 1;
//			memory.write(0x200bff8, mtime, 8, true, false);
		} catch (PageFaultException | IRQException ex) {
			registers.get("mepc").setValue(registers.get("pc").getValue().longValue());
			registers.get("pc").setValue(registers.get("mtvec").getValue().longValue() & 0xfffffffffffffffcL);
			registers.get("mcause").setValue(0xd); //this is wrong
			CPUState.setPriv(3);
		} catch (WrongInstructionException ex) {
			System.out.println(ex.getMessage());
			System.exit(6000);
		} catch (NoOfByteException ex) {
			System.exit(6000);
		} catch (AccessFaultException ex) {
			System.exit(6000);
		}
		cpuTick++;
	}

	private FireInterrupt handleInterrupt() {
		FireInterrupt fireInterrupt = Setting.getInstance().getFireInterrupt(cpuTick);
		if (fireInterrupt != null) {
			long cause = fireInterrupt.cause & 0x7fffffffffffffffl;
			long mask = 1 << cause;
			if ((CPUState.priv == CPUState.MACHINE_MODE && (registers.get("mie").getValue().longValue() & mask) == mask && (registers.get("mip").getValue().longValue() & mask) == 0x0)
					|| (CPUState.priv == CPUState.SUPERVISOR_MODE && (registers.get("sie").getValue().longValue() & mask) == mask && (registers.get("sip").getValue().longValue() & mask) == 0x0)) {
//			if (mtime > mtimecmp || Setting.getInstance().fireInterrupts.contains(cpuTick)) {
				return fireInterrupt;
			}
		}
		return null;
	}

	private void handleTypeI(int arr[], Line line) throws PageFaultException, NoOfByteException, IRQException, AccessFaultException {
		String rd = Registers.getRegXNum32(line.rd);
		String rs1 = Registers.getRegXNum32(line.rs1);
		int shamt5 = (arr[3] & 0x1) << 4 | (arr[2] & 0xf0) >> 4;
		int shamt6 = (arr[3] & 0x3) << 4 | (arr[2] & 0xf0) >> 4;
		long long_val;
		if (line.instruction.equals("addi")) {
			if (rs1.equals("zero")) {
				registers.get(rd).setValue(signExtend64(line.imm, 11));
			} else {
				long_val = registers.get(rs1).getValue().longValue() + signExtend64(line.imm, 11);
				registers.get(rd).setValue(long_val);
			}
		} else if (line.instruction.equals("andi")) {
			long_val = registers.get(rs1).getValue().longValue() & signExtend64(line.imm, 11);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("ori")) {
			long_val = registers.get(rs1).getValue().longValue() | signExtend64(line.imm, 11);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("xori")) {
			long_val = registers.get(rs1).getValue().longValue() ^ line.imm;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("slli")) {
			long_val = registers.get(rs1).getValue().longValue() << shamt6;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("srli")) {
			long_val = registers.get(rs1).getValue().longValue() >>> shamt6;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("srai")) {
			long_val = registers.get(rs1).getValue().longValue() >> shamt6;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("slliw")) {
			long_val = registers.get(rs1).getValue().longValue() << shamt5;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("srliw")) {
			long_val = registers.get(rs1).getValue().longValue() >>> shamt5;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("sraiw")) {
			long_val = registers.get(rs1).getValue().longValue() >> shamt5;
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("slti")) {
			if (registers.get(rs1).getValue().longValue() < line.imm) {
				registers.get(rd).setValue(1);
			} else {
				registers.get(rd).setValue(0);
			}
		} else if (line.instruction.equals("sltiu")) {
			if (registers.get(rs1).getValue().compareTo(new BigInteger(Long.toString(line.imm))) == -1) {
				registers.get(rd).setValue(1);
			} else {
				registers.get(rd).setValue(0);
			}
		} else if (line.instruction.equals("lb")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long_val = memory.read(long_val, 1, true, false);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("lh")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long_val = memory.read(long_val, 2, true, false);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("lbu")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long_val = memory.read(long_val, 1, true, false);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("lhu")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long_val = memory.read(long_val, 2, true, false);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("lw")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long_val = memory.read(long_val, 4, true, false);
			if (((long_val >> 31) & 1) == 1) {
				long_val = long_val | 0xffffffff00000000l;
			}
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("ld")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			long_val = memory.read(long_val, 8, true, false);
			registers.get(rd).setValue(long_val);
		} else if (line.instruction.equals("addiw")) {
			long_val = registers.get(rs1).getValue().longValue() + line.imm;
			registers.get(rd).setValue(signExtend32To64(BigInteger.valueOf(long_val)));
		}
		modifyPC();
	}

	private void handleTypeS(Line line) throws PageFaultException, NoOfByteException, IRQException, AccessFaultException {
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rs2 = Registers.getRegXNum32(line.rs2);

		if (line.instruction.equals("sb")) {
			long address = registers.get(rs2).getValue().longValue();
			memory.write(address + line.imm, registers.get(rs1).getValue().longValue(), 1, false, true);
		} else if (line.instruction.equals("sh")) {
			long address = registers.get(rs2).getValue().longValue();
			memory.write(address + line.imm, registers.get(rs1).getValue().longValue(), 2, false, true);
		} else if (line.instruction.equals("sw")) {
			long address = registers.get(rs2).getValue().longValue();
			memory.write(address + line.imm, registers.get(rs1).getValue().longValue(), 4, false, true);
		} else if (line.instruction.equals("sd")) {
			long address = registers.get(rs2).getValue().longValue();
			memory.write(address + line.imm, registers.get(rs1).getValue().longValue(), 8, false, true);
		}
		modifyPC();
	}

	private void handleTypeB(Line line) throws NoOfByteException, PageFaultException, IRQException {
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rs2 = Registers.getRegXNum32(line.rs2);
		boolean modPC = false;

		if (line.instruction.equals("beq") || line.instruction.equals("beqz")) {
//			System.out.println(" " + registers.get(rs1).getValue().longValue() + " " + registers.get(rs2).getValue().longValue() + " " + line.imm);
			if (registers.get(rs1).getValue().longValue() == registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instruction.equals("bne")) {
			if (registers.get(rs1).getValue().longValue() != registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instruction.equals("bnez")) {
			if (registers.get(rs1).getValue().longValue() != 0) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instruction.equals("blt") || line.instruction.equals("bltz") || line.instruction.equals("bgtz")) {
			if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instruction.equals("bge") || line.instruction.equals("bgez") || line.instruction.equals("blez")) {
			if (registers.get(rs1).getValue().longValue() >= registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instruction.equals("bltu")) {
			if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		} else if (line.instruction.equals("bgeu")) {
			if (registers.get(rs1).getValue().longValue() >= registers.get(rs2).getValue().longValue()) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				modPC = true;
			}
		}
		if (!modPC) {
			modifyPC();
		}
	}

	private void handleTypeR(Line line) throws PageFaultException, NoOfByteException, IRQException, AccessFaultException {
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rs2 = Registers.getRegXNum32(line.rs2);
		String rd = Registers.getRegXNum32(line.rd);
		if (line.instruction.equals("sll")) {
			long val = registers.get(rs1).getValue().longValue() << registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("srl")) {
			long val = registers.get(rs1).getValue().longValue() >>> registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("sra")) {
			long val = registers.get(rs1).getValue().longValue() >> registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("add")) {
			long val = (registers.get(rs1).getValue().longValue() + registers.get(rs2).getValue().longValue()) & 0xffffffffL;
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("sub")) {
			long val;
//			if (registers.get(rs1).getValue().longValue() > registers.get(rs2).getValue().longValue()) {
			val = registers.get(rs1).getValue().longValue() - registers.get(rs2).getValue().longValue();
//			} else {
//				val = 0;
//			}
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("xor")) {
			long val = registers.get(rs1).getValue().longValue() ^ registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("or")) {
			long val = registers.get(rs1).getValue().longValue() | registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("and")) {
			long val = registers.get(rs1).getValue().longValue() & registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("slt")) {
			if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
				registers.get(rd).setValue(1);
			} else {
				registers.get(rd).setValue(0);
			}
		} else if (line.instruction.equals("sltu")) {
			if (rs1.equals("zero")) { // instruction snez
				if (registers.get(rs2).getValue().longValue() == 0) {
					registers.get(rd).setValue(0);
				} else {
					registers.get(rd).setValue(1);
				}
			} else if (registers.get(rs1).getValue().longValue() < registers.get(rs2).getValue().longValue()) {
				registers.get(rd).setValue(1);
			} else {
				registers.get(rd).setValue(0);
			}
		} else if (line.instruction.equals("mul")) {
			long val = (registers.get(rs1).getValue().longValue() * registers.get(rs2).getValue().longValue()) & 0xffffffff;
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("mulh")) {
			long val = (registers.get(rs1).getValue().longValue() * registers.get(rs2).getValue().longValue()) & 0xffffffff00000000L;
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("mulhsu")) {
			long val = (registers.get(rs1).getValue().multiply(registers.get(rs2).getValue())).and(new BigInteger("0xffffffff00000000", 16)).longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("mulhu")) {
			long val = (registers.get(rs1).getValue().multiply(registers.get(rs2).getValue())).and(new BigInteger("0xffffffff00000000", 16)).longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("div")) {
			long val = registers.get(rs1).getValue().longValue() / registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("divu")) {
			long val = registers.get(rs1).getValue().divide(registers.get(rs2).getValue()).longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("rem")) {
			long val = registers.get(rs1).getValue().longValue() % registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("remu")) {
			long val = registers.get(rs1).getValue().mod(registers.get(rs2).getValue()).longValue();
			registers.get(rd).setValue(val);
		} else if (line.instruction.equals("lr.w")) { //32A Standard Extension

		} else if (line.instruction.equals("sc.w")) {

		} else if (line.instruction.equals("amoswap.w.aq")) {
			long long_val = registers.get(rs1).getValue().longValue();
			long val = registers.get(rs2).getValue().longValue();
			long_val = memory.read(long_val, 8, true, false);
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(long_val);
			}
			memory.write(registers.get(rs1).getValue().longValue(), val, 1, false, true);
		} else if (line.instruction.equals("amoswap.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			long rs2Value = registers.get(rs2).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
			}
			memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
		} else if (line.instruction.equals("amoadd.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() + rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoxor.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() ^ rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoand.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() & rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoor.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() | rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amomin.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amomax.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amominu.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amomaxu.w")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("lr.d")) { //64A Standard Extension

		} else if (line.instruction.equals("sc.d")) {

		} else if (line.instruction.equals("amoswap.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			long rs2Value = registers.get(rs2).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoadd.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() + rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoxor.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() ^ rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoand.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() & rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amoor.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = registers.get(rs2).getValue().longValue() | rs1Value;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amomin.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amomax.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amominu.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() >= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("amomaxu.d")) {
			long rs1Value = registers.get(rs1).getValue().longValue();
			rs1Value = memory.read(rs1Value, 8, true, false);
			long rs2Value = (registers.get(rs2).getValue().longValue() <= rs1Value) ? rs1Value : registers.get(rs2).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value);
				memory.write(registers.get(rs1).getValue().longValue(), rs2Value, 1, false, true);
			}
		} else if (line.instruction.equals("remuw")) {
			long rs1Value = registers.get(rs1).getValue().longValue() & 0xffffffffl;
			long rs2Value = registers.get(rs2).getValue().longValue() & 0xffffffffl;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value % rs2Value);
			}
		} else if (line.instruction.equals("muluw")) {
			long rs1Value = registers.get(rs1).getValue().longValue() & 0xffffffffl;
			long rs2Value = registers.get(rs2).getValue().longValue() & 0xffffffffl;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value * rs2Value);
			}
		} else if (line.instruction.equals("divuw")) {
			long rs1Value = registers.get(rs1).getValue().longValue() & 0xffffffffl;
			long rs2Value = registers.get(rs2).getValue().longValue() & 0xffffffffl;
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(rs1Value / rs2Value);
			}
		} else if (line.instruction.equals("subw")) {
			long val;
			val = registers.get(rs1).getValue().and(BigInteger.valueOf(0xffffffffl)).longValue() - registers.get(rs2).getValue().and(BigInteger.valueOf(0xffffffffl)).longValue();
			registers.get(rd).setValue(val);
			registers.get(rd).setValue(signExtend32To64(registers.get(rd).getValue()));
		} else {
			System.out.println("FUCK, no way to execute");
			System.out.println(line);
			System.exit(1000);
		}

		modifyPC();
	}

	private void handleTypeU(Line line) throws NoOfByteException, PageFaultException, IRQException {
		String rd = Registers.getRegXNum32(line.rd);
		if (line.instruction.equals("lui")) {
			long value = signExtend64(line.imm << 12, 31);
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("auipc")) {
			//registers.get(rd).value = registers.et("pc").value + value1;
			long value = registers.get("pc").getValue().longValue() + (line.imm << 12);//pc relative address
			value = value & 0xffffffffl;
			registers.get(rd).setValue(value);
		}

		modifyPC();
	}

	private void handleCityu(Line line) throws PageFaultException, NoOfByteException, IRQException {
		String rd = Registers.getRegXNum32(line.rd);
		String rs1 = Registers.getRegXNum32(line.rs1);

		long value;
		value = registers.get(rs1).getValue().longValue();
		value *= value;
		registers.get(rd).setValue(value);

		modifyPC();
	}

	private void handleEcallOrTrap(Line line) throws NoOfByteException, PageFaultException, IRQException {
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rd = Registers.getRegXNum32(line.rd);
		String csr = Registers.getCsrNum12(line.csr);

		if (line.instruction.equals("ecall")) {
			registers.get("mepc").setValue(registers.get("pc").getValue().longValue());
			registers.get("pc").setValue(registers.get("mtvec").getValue().longValue() & 0xfffffffffffffffcL);
			registers.get("mcause").setValue(0xb); //machine external interrupt
		} else if (line.instruction.equals("ebreak")) {

		} else if (line.instruction.equals("wfi")) {
			int mie = (int) registers.get("mstatus").getValue().longValue() & 0b1000;
			int hie = (int) registers.get("mstatus").getValue().longValue() & 0b0100;
			int sie = (int) registers.get("mstatus").getValue().longValue() & 0b0010;
			int uie = (int) registers.get("mstatus").getValue().longValue() & 0b0001;

			if (mie == 8 || hie == 4 || sie == 2 || uie == 1) {
				registers.get("mepc").setValue(registers.get("pc").getValue().longValue() + 4);
			}
			modifyPC();

		} else if (line.instruction.equals("csrrw")) {
			/* The CSRRW (Atomic Read/Write CSR) instruction atomically swaps values in the CSRs and integer registers. 
			CSRRW reads the old value of the CSR, zero-extends the value to XLEN bits, then writes it to integer 
			register rd. The initial value in rs1 is written to the CSR. If rd=x0, then the instruction shall not 
			read the CSR and shall not cause any of the side effects that might occur on a CSR read. 
			A CSRRW with rs1=x0 will attempt to write zero to the destination CSR.*/
//			csr = CSR.csrToUse(csr);
			long csr_val = registers.get(csr).getValue().longValue();
			long rs1_val = registers.get(rs1).getValue().longValue();
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(csr_val);
			}
			if (csr.equals("mideleg")) { //temporarily, should be wrong becuz too weird
				registers.get(csr).setValue((csr_val & ~CSR.MIDELEG_MASK) | (rs1_val & CSR.MIDELEG_MASK));
			} else {
				CSR.checkWritePermission(csr);
				registers.get(csr).setValue(rs1_val & CSR.getMask(csr));
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(csr);
				}
			}
			modifyPC();
		} else if (line.instruction.equals("csrrs")) {
			/* The CSRRS (Atomic Read and Set Bits in CSR) instruction reads the value of the CSR, zero-extends 
			the value to XLEN bits, and writes it to integer register rd. The initial value in integer register 
			rs1 is treated as a bit mask that specifies bit positions to be set in the CSR. Any bit that is high 
			in rs1 will cause the corresponding bit to be set in the CSR, if that CSR bit is writable. Other bits 
			in the CSR are not explicitly written. 
			For both CSRRS and CSRRC, if rs1=x0, then the instruction will not write to the CSR at all, and so shall 
			not cause any of the side effects that might otherwise occur on a CSR write, nor raise illegal instruction 
			exceptions on accesses to read-only CSRs. Both CSRRS and CSRRC always read the addressed CSR and cause any 
			read side effects regardless of rs1 and rd fields. Note that if rs1 specifies a register holding a zero value 
			other than x0, the instruction will still attempt to write the unmodified value back to the CSR and will cause 
			any attendant side effects.
			 */
//			csr = CSR.csrToUse(csr);
			long csr_val = registers.get(csr).getValue().longValue();
			long rs1_val = registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(csr_val);
			if (!rs1.equals("zero")) {
				CSR.checkWritePermission(csr);
				registers.get(csr).setValue((csr_val | rs1_val) & CSR.getMask(csr));
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(csr);
				}
			}
			modifyPC();

		} else if (line.instruction.equals("csrrc")) {
			/* The CSRRC (Atomic Read and Clear Bits in CSR) instruction reads the value of the CSR, zero-extends the value to XLEN
			bits, and writes it to integer register rd. The initial value in integer register rs1 is treated as a bit mask that specifies
			bit positions to be cleared in the CSR. Any bit that is high in rs1 will cause the corresponding bit to be cleared in the CSR,
			if that CSR bit is writable. Other bits in the CSR are not explicitly written. If rs1 is x0, check conditions in above CSRRS. */
//			csr = CSR.csrToUse(csr);
			long csr_val = registers.get(csr).getValue().longValue(), rs1_val = registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(csr_val);
			if (!rs1.equals("zero")) {
				CSR.checkWritePermission(csr);
				registers.get(csr).setValue(csr_val & (~rs1_val) & CSR.getMask(csr));
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(csr);
				}
			}
			modifyPC();
		} else if (line.instruction.equals("csrrwi")) {
			if (!rd.equals("zero")) {
				registers.get(rd).setValue(registers.get(csr).getValue().longValue());
			}
			if (registers.get(csr).getName().equals("read_only")) {
				System.out.println("Error: Writing to a Read only Register");
			} else {
				registers.get(csr).setValue(line.imm);
			}
			if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
				PMP.updatePmp(csr);
			}
			modifyPC();
		} else if (line.instruction.equals("csrrsi")) {
			registers.get(rd).setValue(registers.get(csr).getValue().longValue());
			if (line.imm != 0) {
				if (registers.get(csr).getName().equals("read_only")) {
					System.out.println("Error: Writing to a Read only Register");
				} else {
					registers.get(csr).setValue(registers.get(csr).getValue().longValue() | line.imm);
				}
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(csr);
				}
			}
			modifyPC();
		} else if (line.instruction.equals("csrrci")) {
			registers.get(rd).setValue(registers.get(csr).getValue().longValue());
			if (line.imm != 0) {
				if (registers.get(csr).getName().equals("read_only")) {
					System.out.println("Error: Writing to a Read only Register");
				} else {
					registers.get(csr).setValue(registers.get(csr).getValue().longValue() & (~line.imm));
				}
				if (csr.startsWith("pmp")) { //handle pmp after the change of csr value. Can further optimize to update specific pmp address conf number (tbc)
					PMP.updatePmp(csr);
				}
			}
			modifyPC();
		} else if (line.instruction.equals("mret")) {
			long mpp = CommonLib.getValue(registers.get("mstatus").getValue().longValue(), 11, 12);
			if (mpp == 0) {
				CPUState.setPriv(0);
			} else if (mpp == 1) {
				CPUState.setPriv(1);
			} else if (mpp == 2) {
				CPUState.setPriv(2);
			} else if (mpp == 3) {
				CPUState.setPriv(3);
			}
			registers.get("pc").setValue(registers.get("mepc").getValue().longValue());
			long mstatus = registers.get("mstatus").getValue().longValue();
			//0x808-->0x800 
			long mpie = CommonLib.getValue(registers.get("mstatus").getValue().longValue(), 7, 7);
			long mpie_if_0 = 0xfffffff7;
			long mpie_if_1 = 0x8;

			if (mpie == 1) {
				mstatus = mstatus | mpie_if_1;
			} else if (mpie == 0) {
				mstatus = mstatus & mpie_if_0;
			}
			//mpie-->1
			mstatus = mstatus | 0x00000080;
			//mpp-->prv_u(0);
			mstatus = mstatus & 0xffffffffffffE7ffl;
			//mpv-->0
			mstatus = mstatus & 0xffffff7fffffffffl;
			registers.get("mstatus").setValue(BigInteger.valueOf(mstatus));
		} else if (line.instruction.equals("uret")) {
			System.out.println("not support " + line.code);
			System.exit(123);
		} else if (line.instruction.equals("sret")) {
			long spp = CommonLib.getValue(registers.get("sstatus").getValue().longValue(), 8, 8);
			if (spp == 0) {
				CPUState.setPriv(CPUState.USER_MODE);
			} else if (spp == 1) {
				CPUState.setPriv(CPUState.SUPERVISOR_MODE);
			}

			registers.get("pc").setValue(registers.get("sepc").getValue().longValue());
//			long sstatus = registers.get("sstatus").value.longValue();
//			long spie = CommonLib.getValue(registers.get("sstatus").value.longValue(), 5, 5);
//			long sie = spie;
//			sie = sie & 0xfL;
//			sstatus = sstatus | sie;
//			spie = 0x20L;
//			sstatus = sstatus | spie;
//			registers.get("sstatus").setValue(BigInteger.valueOf(sstatus));

			long mstatus = registers.get("mstatus").getValue().longValue();
			mstatus = mstatus & 0xfffffffffffffeffl;
			mstatus = mstatus | 2;
			registers.get("mstatus").setValue(BigInteger.valueOf(mstatus));

			long sstatus = registers.get("sstatus").getValue().longValue();
			sstatus = sstatus & 0xfffffffffffffeffl;
			sstatus = sstatus | 2;
			registers.get("sstatus").setValue(BigInteger.valueOf(sstatus));

//          should change the mstatus too (havn't finish);
		} else if (line.instruction.equals("hret")) {
			System.out.println("not support " + line.code);
			System.exit(123);
		} else if (line.instruction.equals("sfence.vm")) {
			modifyPC();
		} else {
			logger.log(Level.SEVERE, "unhandle : " + line.code);
			return;
		}

	}

	private void handleCompressed(Line line) throws NoOfByteException, ArrayIndexOutOfBoundsException, PageFaultException, IRQException, AccessFaultException {
		boolean branched = false;

		if ((line.instruction.equals("c.lui") && line.rd == 2) || line.instruction.equals("c.addi16sp")) {
			if (line.imm != 0) {
				registers.get("sp").setValue(registers.get("sp").getValue().longValue() + line.imm);
			}
		} else if (line.instruction.equals("c.addi4spn")) {
//			if (line.imm != 0) {
			String rd = Registers.getRegXNum16(line.rd);
			long imm = line.imm;
			long value = registers.get("sp").getValue().longValue() + imm;
			registers.get(rd).setValue(value);
//			}

		} else if (line.instruction.equals("c.add")) {
			String rd = Registers.getRegXNum32(line.rd);
			String rs1 = Registers.getRegXNum32(line.rs1);
			long value = registers.get(rd).getValue().longValue() + registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.addi")) {
			String rd = Registers.getRegXNum32(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() + imm;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.and")) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rd).getValue().longValue() & registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.andi")) {
			String rd = Registers.getRegXNum16(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() & imm;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.or")) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rd).getValue().longValue() | registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.xor")) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rd).getValue().longValue() ^ registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.sub")) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rd).getValue().longValue() - registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.mv")) {
			String rd = Registers.getRegXNum32(line.rd);
			String rs1 = Registers.getRegXNum32(line.rs1);
			long value = registers.get(rs1).getValue().longValue();
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.li")) {
			String rd = Registers.getRegXNum32(line.rd);
			long imm = line.imm;
			long value = registers.get("zero").getValue().longValue() + imm;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.lui")) {
			String rd = Registers.getRegXNum32(line.rd);
			registers.get(rd).setValue(signExtend64(line.imm << 12, 31));
		} else if (line.instruction.equals("c.slli")) {
			String rd = Registers.getRegXNum32(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() << imm;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.srai")) {
			String rd = Registers.getRegXNum16(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() >> imm;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.srli")) {
			String rd = Registers.getRegXNum16(line.rd);
			long imm = line.imm;
			long value = registers.get(rd).getValue().longValue() >>> imm;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.beqz")) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			if (registers.get(rs1).getValue().longValue() == 0) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				branched = true;
			}
		} else if (line.instruction.equals("c.bnez")) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			if (registers.get(rs1).getValue().longValue() != 0) {
				registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
				branched = true;
			}
		} else if (line.instruction.equals("c.sw")) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			String rs2 = Registers.getRegXNum16(line.rs2);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
//			registers.get(rs1).value = (long) memory.get(value) & 0xffffffff;
			memory.write(value, registers.get(rs2).getValue().longValue(), 4, false, true);
		} else if (line.instruction.equals("c.swsp")) {
			String rs2 = Registers.getRegXNum32(line.rs2);
			long value = registers.get("sp").getValue().longValue() + line.imm;
			System.out.println("write" + Long.toHexString((registers.get(rs2).getValue().longValue() & 0xff)) + "to" + Long.toHexString(value));
//			registers.get(rs2).value = (long) memory.get(value) & 0xffffffff;  System.out.println("write" + Integer.toHexString((int) (registers.get(rs2).value & 0xff)) + "to" + Integer.toHexString(int_val));
			// System.out.println("write" + Integer.toHexString((int) (registers.get(rs2).value & 0xff00)) + "to" + Integer.toHexString(int_val));
			memory.write(value, registers.get(rs2).getValue().longValue(), 4, false, true);
		} else if (line.instruction.equals("c.fsw")) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			String rs2 = Registers.getRegFNum16(line.rs2);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
//			registers.get(rs1).value = (long) memory.get(value) & 0xffffffff;
			memory.write(value, registers.get(rs2).getValue().longValue(), 4, false, true);
//		} else if (line.instruction.equals("c.fswsp")) {
//			rs2 = Registers.getRegFNum32(line.rs2);
//			long_val = registers.get("sp").value.longValue() + ((int) line.imm);
////			registers.get(rs2).value = (long) memory.get(value) & 0xffffffff;
//			rd = Registers.getRegFNum32(line.rd);
//			if(rd.equals("ft1")){
//				memory.writeMemory(long_val, registers.get(rs2).getValue().longValue(), 4);			
//			}
		} else if (line.instruction.equals("c.fsd")) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			String rs2 = Registers.getRegFNum16(line.rs2);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
//			registers.get(rs1).value = (long) memory.get(value) & 0xffffffff;
			memory.write(value, registers.get(rs2).getValue().longValue(), 8, false, true);
		} else if (line.instruction.equals("c.sdsp")) {
			String rs2 = Registers.getRegXNum32(line.rs2);
			long value = registers.get("sp").getValue().longValue() + line.imm;
			memory.write(value, registers.get(rs2).getValue().longValue(), 8, false, true);
		} else if (line.instruction.equals("c.sd")) {
			String rs1 = Registers.getRegXNum16(line.rs1);
			String rs2 = Registers.getRegXNum16(line.rs2);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
			memory.write(value, registers.get(rs2).getValue().longValue(), 8, false, true);
			if (value == 0x2004000L) {
				registers.get("mip").setValue(registers.get("mip").getValue().longValue() & 0xf);
			}
		} else if (line.instruction.equals("c.j")) {
			//registers.get().value = registers.get(rd).value >> imm;
			registers.get("pc").setValue(registers.get("pc").getValue().longValue() + line.imm);
			branched = true;
		} else if (line.instruction.equals("c.jr")) {
			String rd = Registers.getRegXNum32(line.rd);
			registers.get("pc").setValue(registers.get(rd).getValue().longValue());
			if (line.code.equals("sret")) {
				if (CPUState.priv == CPUState.SUPERVISOR_MODE) {
					long mstatus = registers.get("mstatus").getValue().longValue();
					mstatus = mstatus & 0xfffffeffl;
					mstatus = mstatus | 2;
					registers.get("mstatus").setValue(BigInteger.valueOf(mstatus));

					long sstatus = registers.get("sstatus").getValue().longValue();
					sstatus = sstatus & 0xfffffeffl;
					sstatus = sstatus | 2;
					registers.get("sstatus").setValue(BigInteger.valueOf(sstatus));
				}
			}
			branched = true;
		} else if (line.instruction.equals("c.lw")) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
			value = memory.read(value, 4, true, false);
			registers.get(rd).setValue(signExtend32To64(BigInteger.valueOf(value)));
		} else if (line.instruction.equals("c.lwsp")) {
			String rd = Registers.getRegXNum32(line.rd);
			long value = registers.get("sp").getValue().longValue() + line.imm;
			if (!rd.equals("zero") && (line.imm % 4 == 0)) {
				value = memory.read(value, 4, true, false);
				registers.get(rd).setValue(value);
			}
		} else if (line.instruction.equals("c.flw")) {
			String rd = Registers.getRegFNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
			if (line.imm % 4 == 0) {
				value = memory.read(value, 4, true, false);
				registers.get(rd).setValue(value);
			}

		} else if (line.instruction.equals("c.ld")) {
			String rd = Registers.getRegXNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
			value = memory.read(value, 8, true, false);
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.ldsp")) {
			String rd = Registers.getRegXNum32(line.rd);
			long value = registers.get("sp").getValue().longValue() + line.imm;
			value = memory.read(value, 8, true, false); //(memory.read(long_val + 3) & 0xff) << 24 | (memory.read(long_val + 2) & 0xff) << 16 | (memory.read(long_val + 1) & 0xff) << 8 | memory.read(long_val) & 0xff;
			registers.get(rd).setValue(value);
		} else if (line.instruction.equals("c.fld")) {
			String rd = Registers.getRegFNum16(line.rd);
			String rs1 = Registers.getRegXNum16(line.rs1);
			long value = registers.get(rs1).getValue().longValue() + line.imm;
			if (line.imm % 8 == 0) {
				value = memory.read(value, 8, true, false);
				registers.get(rd).setValue(value);
			}
		} else if (line.instruction.equals("c.fldsp")) {
			String rd = Registers.getRegFNum32(line.rd);
			long value = registers.get("sp").getValue().longValue() + line.imm;

			if (line.imm % 8 == 0) {
				value = memory.read(value, 8, true, false);
				registers.get(rd).setValue(value);
			}
		} else if (line.instruction.equals("c.addiw")) {
			registers.get("pc").setValue(registers.get("pc").getValue().longValue() + 2);
			String rd = Registers.getRegXNum32(line.rd);
			long value = registers.get(rd).getValue().longValue() + line.imm;
			registers.get(rd).setValue(value);
			branched = true;
		} else if (line.instruction.equals("c.jalr")) {
			registers.get("ra").setValue(registers.get("pc").getValue().longValue() + 2);
			registers.get("pc").setValue(registers.get("rs1").getValue().longValue() + line.imm);
			branched = true;
		} else if (line.instruction.equals("c.ebreak")) {
		} else if (line.instruction.equals("c.subw")) {
			String rs2 = Registers.getRegXNum32(line.rs2);
			String rd = Registers.getRegXNum32(line.rd);
			long val = registers.get(rd).getValue().longValue() - registers.get(rs2).getValue().longValue();
			registers.get(rd).setValue(val);
		} else {
			System.err.println("unhandled instruction " + line.instruction + ", cpuTick=" + cpuTick);
			System.exit(3);
		}

		if (!branched) {
			modifyPC();
		}
	}

	private void handleTypeV(int arr[], Line line) throws ArrayIndexOutOfBoundsException, PageFaultException, NoOfByteException, IRQException {
		String rd = Registers.getRegXNum32(line.rd);
		String rs1 = Registers.getRegXNum32(line.rs1);
		String rs2 = Registers.getRegXNum32(line.rs2);
		String vd = Registers.getVregNum32(line.vd);
		String vs1 = Registers.getVregNum32(line.vs1);
		String vs2 = Registers.getVregNum32(line.vs2);
		String vs3 = Registers.getVregNum32(line.vs3);
		String vma = Registers.getVregNum32(line.vma);
		String vta = Registers.getVregNum32(line.vta);
		String vsew = Registers.getVregNum32(line.vsew);
		String vlmul = Registers.getVregNum32(line.vlmul);
		int imm = (arr[3] << 4) | ((arr[2] & 0xf0) >> 4);
		int shamt = (arr[3] & 0x01) << 4 | (arr[2] & 0xf0) >> 4;
		long long_val;
		int int_val;
		if (line.instruction.equals("vadd.vv")) {
//            System.out.println("vs1 = " + vs1);
//            System.out.println(vs1 + " = " + registers.get(vs1).getValue());
//            System.out.println("vs2 = " + vs2);
//            System.out.println(vs2 + " = " + registers.get(vs2).getValue());
//            System.out.println("vd = " + vd);
//            System.out.println(vd + " = " + registers.get(vd).getValue());
			long_val = registers.get(vs2).getValue().longValue() + registers.get(vs1).getValue().longValue();
			registers.get(vd).setValue(long_val);
		} else if (line.instruction.equals("vle32.v")) {

		}
		modifyPC();
	}

	public long Intpow2(int b) {
		long sum = 1;
//		System.out.println("b" + b);
		for (int i = 0; i < b; i++) {
			sum *= 2;
//			System.out.println(sum);
		}

		return sum;
	}

	public long Sign32ToUnsign32(String s) {
		long a = 0;

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '1') {

				a += Intpow2(s.length() - i - 1);

			}
		}
		return a;
	}

	public long signExtend64(long val, int signbit) { //Input the value, and the bit to sign extend.
		return (((val >> signbit) & 1) == 1) ? val | (0xffffffffffffffffL << signbit) : val & (0xffffffffffffffffL >>> (63 - signbit));
	}

	private String serializeHashmap(HashMap<String, Object> map, boolean appendComma) {
		StringBuilder sb = new StringBuilder();
		if (appendComma) {
			sb.append(",");
		}
		sb.append("{");

		sb.append("\"line\": \"" + map.get("line") + "\",");

		sb.append("\"bytes\": [");
		int[] bytes = (int[]) map.get("bytes");
		for (int x = 0; x < bytes.length; x++) {
			int b = bytes[x];
			if (x > 0) {
				sb.append(",");
			}
			sb.append("" + b + "");
		}
		sb.append("],");

		sb.append("\"registers\": {");
		LinkedHashMap<String, GeneralRegister> registers = (LinkedHashMap<String, GeneralRegister>) map.get("registers");
		int x = 0;
		for (String key : registers.keySet()) {
			if (x > 0) {
				sb.append(",");
			}
			sb.append("\"" + key + "\":{");
			sb.append("\"name\":\"" + key + "\",");
			sb.append("\"value\":" + registers.get(key).value + "");
			sb.append("}");
			x++;
		}
		sb.append("},");

		sb.append("\"memoryOperands\": [");
		ArrayList<MemoryOperand> memoryOperands = (ArrayList<MemoryOperand>) map.get("memoryOperands");
		synchronized (memoryOperands) {
			x = 0;
			for (MemoryOperand memoryOperand : memoryOperands) {
//			System.out.println(memoryOperand);
				if (x > 0) {
					sb.append(",");
				}
				sb.append(" {");
				sb.append("\"operand\": \"" + memoryOperand.operand + "\",");
				sb.append("\"offset\": " + memoryOperand.offset + ",");
				sb.append("\"offsetAfterMapping\": " + memoryOperand.offsetAfterMapping + ",");
				sb.append("\"b\": " + memoryOperand.b + "");
				sb.append("}");
				x++;
			}
		}
		sb.append("]");
		sb.append("}");
		return sb.toString();
	}

	//zzz
	private Line getLine(long pc) {
		Line line = null;
		try {
			int[] ins;
			int noOfByte = RISCVDisassembler.getNoOfByte(memory.read(pc, true, false));
			if (noOfByte == 4) {
				ins = new int[4];
				ins[0] = memory.read(pc, true, false) & 0xff;
				ins[1] = memory.read(pc + 1, true, false) & 0xff;
				ins[2] = memory.read(pc + 2, true, false) & 0xff;
				ins[3] = memory.read(pc + 3, true, false) & 0xff;
			} else {
				ins = new int[2];
				ins[0] = memory.read(pc, true, false) & 0xff;
				ins[1] = memory.read(pc + 1, true, false) & 0xff;
			}

			// get line object			
			String type = RISCVDisassembler.getType(memory.read(pc, true, false) & 0x7F);
			if (type.equals("decodeTypeB")) {
				line = riscvDisassembler.decodeTypeB(ins);
			} else if (type.equals("decodeTypeI")) {
				line = riscvDisassembler.decodeTypeI(ins);
			} else if (type.equals("decodeTypeS")) {
				line = riscvDisassembler.decodeTypeS(ins);
			} else if (type.equals("decodeTypeR")) {
				line = riscvDisassembler.decodeTypeR(ins);
			} else if (type.equals("decodeTypeU")) {
				line = riscvDisassembler.decodeTypeU(ins);
			} else if (type.equals("ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU")) {
				line = riscvDisassembler.ecallOrEbreakOrCsrOrTrapOrInterruptOrMMU(ins);
			} else if (type.equals("jal")) {
				line = riscvDisassembler.jal(ins);
			} else if (type.equals("jalr")) {
				line = riscvDisassembler.jalr(ins);
			} else if (type.equals("fence")) {
				line = riscvDisassembler.fence(ins);
			} else if (type.equals("cityu")) {
				line = riscvDisassembler.cityu(ins);
			} else if (type.equals("magic")) {
				line = riscvDisassembler.decodeMagic(ins);
			} else if (type.equals("compressed")) {
				if (ins[0] == 0 && ins[1] == 0) {
					line = riscvDisassembler.decodeWrongType(ins);
				} else {
					line = riscvDisassembler.decodeCompressed(ins);
				}
			}
		} catch (IRQException | PageFaultException e) {
			System.out.println("Page Fault");
		} catch (WrongInstructionException e) {
			System.out.println("Unrecognized Instruction");
		} catch (NoOfByteException e) {
			System.out.println("Bullshit");
		}
		return line;
	}

	private void dumpToJson(long pc, int[] ins) {
		try {
			Line line = getLine(pc);
			HashMap<String, Object> map = new HashMap();
			map.put("line", line.code);
			map.put("bytes", ins);
			map.put("registers", registers);
			map.put("memoryOperands", memory.lastOperands);
			FileUtils.writeStringToFile(new File(dumpJson), serializeHashmap(map, cpuTick > 0), "utf8", true);
			memory.resetLastOperands();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void dumpToLog(int[] opcodes, Line line, long cpuTick) {
		try {
			memory.resetLastOperands();
			logThread.add(cpuTick, line.code, CommonLib.getHexString(opcodes, " "), "ABCD", CPUState.priv, registers);
		} catch (SQLException ex) {
			ex.printStackTrace();
			System.exit(1);
		}
	}

	public void initRegistersAndMemory() throws FileNotFoundException, IOException, PageFaultException, IRQException {
		System.out.println("init registers and memory");
		registers.put("pc", new GeneralRegister("pc", startAddress));

		//General Purpose Registers
		for (int x = 0; x <= 31; x++) {
			registers.put("x" + x, new GeneralRegister("x" + x, 0x0));
		}
		registers.put("zero", registers.get("x0"));
		registers.put("ra", registers.get("x1"));
		registers.put("sp", registers.get("x2"));
		registers.put("gp", registers.get("x3"));
		registers.put("tp", registers.get("x4"));
		registers.put("t0", registers.get("x5"));
		registers.put("t1", registers.get("x6"));
		registers.put("t2", registers.get("x7"));
		registers.put("s0", registers.get("x8"));
		registers.put("s1", registers.get("x9"));
		registers.put("a0", registers.get("x10"));
		registers.put("a1", registers.get("x11"));
		registers.put("a2", registers.get("x12"));
		registers.put("a3", registers.get("x13"));
		registers.put("a4", registers.get("x14"));
		registers.put("a5", registers.get("x15"));
		registers.put("a6", registers.get("x16"));
		registers.put("a7", registers.get("x17"));
		registers.put("s2", registers.get("x18"));
		registers.put("s3", registers.get("x19"));
		registers.put("s4", registers.get("x20"));
		registers.put("s5", registers.get("x21"));
		registers.put("s6", registers.get("x22"));
		registers.put("s7", registers.get("x23"));
		registers.put("s8", registers.get("x24"));
		registers.put("s9", registers.get("x25"));
		registers.put("s10", registers.get("x26"));
		registers.put("s11", registers.get("x27"));
		registers.put("t3", registers.get("x28"));
		registers.put("t4", registers.get("x29"));
		registers.put("t5", registers.get("x30"));
		registers.put("t6", registers.get("x31"));

		//Floating Point Registers
		for (int x = 0; x <= 31; x++) {
			registers.put("f" + x, new GeneralRegister("f" + x, 0x0));
		}
		for (int x = 0; x <= 7; x++) {
			registers.put("ft" + x, registers.get("f" + x));
		}
		for (int x = 0; x <= 1; x++) {
			registers.put("fs" + x, registers.get("f" + (x + 8)));
		}
		for (int x = 0; x <= 7; x++) {
			registers.put("fa" + x, registers.get("f" + (x + 10)));
		}
		for (int x = 2; x <= 11; x++) {
			registers.put("fs" + x, registers.get("f" + (x + 16)));
		}
		for (int x = 8; x <= 11; x++) {
			registers.put("ft" + x, registers.get("f" + (x + 20)));
		}

		//CSR Registers. doc from https://five-embeddev.com/riscv-isa-manual/latest/priv-csrs.html#csrrwpriv
		registers.put("ustatus", new CSRRegister("000", 0x0));
		registers.put("uie", new CSRRegister("004", 0x0));
		registers.put("utvec", new CSRRegister("005", 0x0));
		registers.put("uscratch", new CSRRegister("040", 0x0));
		registers.put("uepc", new CSRRegister("041", 0x0));
		registers.put("ucasue", new CSRRegister("042", 0x0));
		registers.put("utval", new CSRRegister("043", 0x0));
		registers.put("uip", new CSRRegister("044", 0x0));
		registers.put("fflags", new CSRRegister("001", 0x0));
		registers.put("frm", new CSRRegister("002", 0x0));
		registers.put("fcsr", new CSRRegister("003", 0x0));
		registers.put("cycle", new CSRRegister("C00", 0x0));
		registers.put("time", new CSRRegister("C01", 0x0));
		registers.put("instret", new CSRRegister("C02", 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("hpmcounter" + x, new CSRRegister("C0" + Integer.toHexString(x), 0x0));
		}
		registers.put("cycleh", new CSRRegister("C80", 0x0));
		registers.put("timeh", new CSRRegister("C81", 0x0));
		registers.put("instreth", new CSRRegister("C82", 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("hpmcounter" + x + "h", new CSRRegister("C8" + Integer.toHexString(x), 0x0));
		}
		registers.put("sstatus", new CSRRegister("100", 0x0));
		registers.put("sedeleg", new CSRRegister("102", 0x0));
		registers.put("sideleg", new CSRRegister("103", 0x0));
		registers.put("sie", new CSRRegister("104", 0x0));
		registers.put("stvec", new CSRRegister("105", 0x0));
		registers.put("scounteren", new CSRRegister("106", 0x0));
		registers.put("sscratch", new CSRRegister("140", 0x0));
		registers.put("sepc", new CSRRegister("141", 0x0));
		registers.put("scause", new CSRRegister("142", 0x0));
		registers.put("stval", new CSRRegister("143", 0x0));
		registers.put("sip", new CSRRegister("144", 0x0));
		registers.put("satp", new CSRRegister("180", 0x0));
		registers.put("scontext", new CSRRegister("5A8", 0x0));
		registers.put("hstatus", new CSRRegister("600", 0x0));
		registers.put("hedeleg", new CSRRegister("602", 0x0));
		registers.put("hideleg", new CSRRegister("603", 0x0));
		registers.put("hie", new CSRRegister("604", 0x0));
		registers.put("hcounteren", new CSRRegister("606", 0x0));
		registers.put("hgeie", new CSRRegister("607", 0x0));
		registers.put("htval", new CSRRegister("643", 0x0));
		registers.put("hip", new CSRRegister("644", 0x0));
		registers.put("hvip", new CSRRegister("645", 0x0));
		registers.put("htinst", new CSRRegister("64A", 0x0));
		registers.put("hgeip", new CSRRegister("E12", 0x0));
		registers.put("hgatp", new CSRRegister("680", 0x0));
		registers.put("hcontext", new CSRRegister("6A8", 0x0));
		registers.put("htimedelta", new CSRRegister("605", 0x0));
		registers.put("htimedeltah", new CSRRegister("615", 0x0));
		registers.put("vsstatus", new CSRRegister("200", 0x0));
		registers.put("vsie", new CSRRegister("204", 0x0));
		registers.put("vstvec", new CSRRegister("205", 0x0));
		registers.put("vsscratch", new CSRRegister("240", 0x0));
		registers.put("vsepc", new CSRRegister("241", 0x0));
		registers.put("vscause", new CSRRegister("242", 0x0));
		registers.put("vstval", new CSRRegister("243", 0x0));
		registers.put("vsip", new CSRRegister("244", 0x0));
		registers.put("vsatp", new CSRRegister("280", 0x0));
		registers.put("mvendorid", new CSRRegister("F11", 0x0)); //fixed id for hart 
		registers.put("marchid", new CSRRegister("F12", 0x0)); // fixed id for hart
		registers.put("mimpid", new CSRRegister("F13", 0x0));// fixed value 
		registers.put("mhartid", new CSRRegister("F14", 0x0));
		registers.put("mstatus", new CSRRegister("300", 0x0)); // machine mode
		registers.put("misa", new CSRRegister("301", 0x0));
		registers.put("medeleg", new CSRRegister("302", 0x0));
		registers.put("mideleg", new CSRRegister("303", 0x0));
		registers.put("mie", new CSRRegister("304", 0x0));
		registers.put("mtvec", new CSRRegister("305", 0x0));
		registers.put("mcounteren", new CSRRegister("306", 0x0));
		registers.put("mstatush", new CSRRegister("310", 0x0));
		registers.put("mscratch", new CSRRegister("340", 0x0));
		registers.put("mepc", new CSRRegister("341", 0x0));
		registers.put("mcause", new CSRRegister("342", 0x0));
		registers.put("mtval", new CSRRegister("343", 0x0));
		registers.put("mip", new CSRRegister("344", 0x0));
		registers.put("mtinst", new CSRRegister("34A", 0x0));
		registers.put("mtval2", new CSRRegister("34B", 0x0));
		for (int x = 0; x <= 3; x++) {
			registers.put("pmpcfg" + x, new CSRRegister("3A" + Integer.toHexString(x), 0x0));
		}
		for (int x = 0; x <= 15; x++) {
			registers.put("pmpaddr" + x, new CSRRegister(Integer.toHexString(0x3B0 + x), 0x0));
		}
		registers.put("mcycle", new CSRRegister("B00", 0x0));
		registers.put("minstret", new CSRRegister("B02", 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("mhpmcounter" + x, new CSRRegister(Integer.toHexString(0xB00 + x), 0x0));
		}
		registers.put("mcycleh", new CSRRegister("B80", 0x0));
		registers.put("minstreth", new CSRRegister("B82", 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("mhpmcounter" + x + "h", new CSRRegister(Integer.toHexString(0xB80 + x), 0x0));
		}
		registers.put("mcountinhibit", new CSRRegister("320", 0x0));
		for (int x = 3; x <= 31; x++) {
			registers.put("mhpmevent" + x, new CSRRegister(Integer.toHexString(0x320 + x), 0x0));
		}
		registers.put("tselect", new CSRRegister("7A0", 0x0));
		registers.put("tdata1", new CSRRegister("7A1", 0x0));
		registers.put("tdata2", new CSRRegister("7A2", 0x0));
		registers.put("tdata3", new CSRRegister("7A3", 0x0));
		registers.put("mcontext", new CSRRegister("7A8", 0x0));
		registers.put("dcsr", new CSRRegister("7B0", 0x0));
		registers.put("dpc", new CSRRegister("7B1", 0x0));
		registers.put("dscratch0", new CSRRegister("7B2", 0x0));
		registers.put("dscratch1", new CSRRegister("7B3", 0x0));

		//Vector registers
		for (int x = 0; x <= 31; x++) {
			registers.put("v" + x, new GeneralRegister("read_and_write", 0x0));
		}

		//VCSR
		registers.put("vtype", new GeneralRegister("read_and_write", 0x0));
		registers.put("vsew", new GeneralRegister("read_and_write", 0x0));
		registers.put("vlmul", new GeneralRegister("read_and_write", 0x0));
		registers.put("vta", new GeneralRegister("read_and_write", 0x0));
		registers.put("vma", new GeneralRegister("read_and_write", 0x0));
		registers.put("vl", new GeneralRegister("read_and_write", 0x0));
		registers.put("vlenb", new GeneralRegister("read_and_write", 0x0));
		registers.put("vstart", new GeneralRegister("read_and_write", 0x0));
		registers.put("vxrm", new GeneralRegister("read_and_write", 0x0));
		registers.put("vxsat", new GeneralRegister("read_and_write", 0x0));
		registers.put("vcsr", new GeneralRegister("read_and_write", 0x0));

		if (Setting.getInstance() == null) {
			System.err.println("Setting's Instance (xml) is null");
			System.exit(123);
		}

		for (GeneralRegister register : Setting.getInstance().registers) {
			registers.get(register.name).setValue(register.value);
		}

		((CSRRegister) registers.get("mstatus")).linkedRegister = (CSRRegister) registers.get("sstatus");
		((CSRRegister) registers.get("sstatus")).linkedRegister = (CSRRegister) registers.get("mstatus");
		((CSRRegister) registers.get("sie")).linkedRegister = (CSRRegister) registers.get("mie");

		//Memory Init
		for (MemoryMap memoryMap : Setting.getInstance().memoryMaps) {
			memoryMap.ram = new byte[memoryMap.size];
			memory.memoryMaps.put(memoryMap.start, memoryMap);
		}
		for (hk.quantr.riscv_simulator.setting.Memory m : Setting.getInstance().memory) {
			System.out.println(" - init memory " + m.name);
			String arr[] = m.value.split(",");
			int address = 0x1000;
			for (int x = 0; x < arr.length; x++) {
				String s = arr[x].trim();
				byte bytes[] = CommonLib.string2bytes(s);
				for (byte b : bytes) {
					memory.writeByte(address, b, true, false);
					address++;
				}
			}
		}
		PMP.init();
	}

	private void initKernel() throws IOException, PageFaultException, IRQException {
		System.out.println("init kernel");
		if (cmd.hasOption("binary")) {
			File temp = new File(cmd.getOptionValue("binary"));
			byte[] code = new byte[(int) temp.length()];
			try (FileInputStream f1 = new FileInputStream(temp)) {
				f1.read(code);
			}
			long offset = 0x80000000L;
			for (byte b : code) {
				memory.writeByte(offset, b, true, false);
				offset++;
			}
		} else if (cmd.hasOption("e")) {
			String elf = cmd.getOptionValue("e");

//			dwarf = new Dwarf();
//			dwarf.initElf(new File(elf), new File(elf).getName(), 0, false);
			File elfFile = new File(elf);
			if (!elfFile.exists()) {
				System.out.println(elfFile + " not exist");
				System.exit(9000);
			}
			dwarfArrayList = DwarfLib.init(elfFile, 0, false);

			RandomAccessFile rf = new RandomAccessFile(new File(elf), "r");
			for (Dwarf dwarf : dwarfArrayList) {
				for (Elf_Phdr programHeader : dwarf.programHeaders) {
					if (programHeader.getP_type() == 1) {
						System.out.printf("load file from offset 0x%x to memory offset 0x%x\n", programHeader.getP_offset(), programHeader.getP_paddr());
						rf.seek(programHeader.getP_offset().longValue());
						for (int x = 0; x < programHeader.getP_filesz().longValue(); x++) {
							byte b = rf.readByte();
//						System.out.printf("%8x = %x\n", programHeader.getP_paddr().longValue() + x, b);
							memory.writeByte(programHeader.getP_paddr().longValue() + x, b, true, false);
						}
					}
				}
			}
		}
	}

	private void initMemoryHandler() {
		System.out.println("init memory handler");
		UART uart = new UART(true, true, 0x10000000L, 8);
		memoryHandlers.add(uart);

		PLIC plic = new PLIC(true, true, 0xc000000L, 0x210000);
		memoryHandlers.add(plic);

		if (cmd.hasOption("file")) {
			String fsImgPath = cmd.getOptionValue("file");
			File file = new File(fsImgPath);
			if (!file.exists() || !file.isFile() || !file.canRead() || !file.canWrite()) {
				System.err.println(fsImgPath + " not exist/not file/can't read/can't write");
				System.exit(100);
			}
			Virtio virtio = new Virtio(true, true, 0x10001000L, 0xff, memory, file);
			memoryHandlers.add(virtio);
		}
	}

	private void zip(File file) {
		try {
			File archive = new File(file.getName() + ".zip");
			ZipArchiveOutputStream outputStream = new ZipArchiveOutputStream(archive);
			ZipArchiveEntry entry = new ZipArchiveEntry(file, file.getName());
			outputStream.setLevel(5);
			outputStream.setMethod(ZipEntry.DEFLATED);
			entry.setMethod(ZipEntry.DEFLATED);
			outputStream.putArchiveEntry(entry);

			InputStream is = new BufferedInputStream(new FileInputStream(file));
			byte[] buffer = new byte[1024 * 5];
			int len = -1;
			while ((len = is.read(buffer)) != -1) {
				outputStream.write(buffer, 0, len);
			}
			outputStream.closeArchiveEntry();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private BigInteger signExtend32To64(BigInteger value) {
		if (value.testBit(31)) {
			return value.or(BigInteger.valueOf(0xffffffff80000000l));
		} else {
			return value;
		}
	}

	private void handleMemoryHiJack(long cpuTick) throws PageFaultException, IRQException {
		for (MemoryHiJack temp : Setting.getInstance().memoryHiJacks) {
			if (!temp.memRead && cpuTick == temp.sequence && temp.memAddr >= 0xc201000 && temp.memAddr <= 0xc201010) {
				memory.writeByte(temp.memAddr, temp.memValue, true, false);
			}
		}
	}
}
