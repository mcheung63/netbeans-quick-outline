/*
 * Copyright (C) 2018 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hk.quantr.netbeansquickoutline;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyTableModel extends DefaultTableModel {

	public ArrayList<Data> data = new ArrayList<Data>();
	public ArrayList<Data> originalData;

	static String columnNames[];

	static {
		if (ModuleLib.isDebug) {
			columnNames = new String[]{"Type", "Line", "Offset", "Name"};
		} else {
			columnNames = new String[]{"Type", "Line", "Name"};
		}
	}

	void setFilter(String str) {
		if (originalData == null) {
			originalData = new ArrayList<Data>(data);
		}
		if (str == null || str.trim().equals("")) {
			data = new ArrayList<Data>(originalData);
			fireTableStructureChanged();
		} else {
			synchronized (data) {
				data.clear();
				for (Data d : originalData) {
					if (d.name.toLowerCase().contains(str.toLowerCase())) {
						data.add(d);
					}
				}
			}
			fireTableStructureChanged();
		}
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		if (data != null) {
			return data.size();
		} else {
			return 0;
		}
	}

	@Override
	public Object getValueAt(int row, int column) {
		if (ModuleLib.isDebug) {
			if (column == 0) {
				return data.get(row);
			} else if (column == 1) {
				return data.get(row).line;
			} else if (column == 2) {
				return data.get(row).caretPosition;
			} else if (column == 3) {
				return data.get(row).name;
			} else if (column == 999) {
				return data.get(row);
			} else {
				return null;
			}
		} else {
			if (column == 0) {
				return data.get(row);
			} else if (column == 1) {
				return data.get(row).line;
			} else if (column == 2) {
				return data.get(row).name;
			} else if (column == 999) {
				return data.get(row);
			} else {
				return null;
			}
		}
	}

	public Data getDataAt(int row) {
		return data.get(row);
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	}

	@Override
	public Class getColumnClass(int columnIndex) {
		return String.class;
	}
}
