package hk.quantr.netbeansquickoutline.antlr.listener;

import hk.quantr.netbeansquickoutline.Data;
import hk.quantr.netbeansquickoutline.ModuleLib;
import hk.quantr.netbeansquickoutline.Type;
import hk.quantr.netbeansquickoutline.antlr.JavaParser;
import hk.quantr.netbeansquickoutline.antlr.JavaParser.ModifierContext;
import hk.quantr.netbeansquickoutline.antlr.JavaParserBaseListener;
import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyJavaListener extends JavaParserBaseListener {

	public ArrayList<Data> data = new ArrayList<Data>();

//	@Override
//	public void exitClassOrInterfaceModifier(JavaParser.ClassOrInterfaceModifierContext ctx) {
//		System.out.println("type =" + ctx.PUBLIC());
//	}
//
//	@Override
//	public void exitVariableDeclarator(JavaParser.VariableDeclaratorContext ctx) {
//		System.out.println("var =" + ctx.variableDeclaratorId().getText());
//		System.out.println("---------");
//	}
	@Override
	public void exitClassBodyDeclaration(JavaParser.ClassBodyDeclarationContext ctx) {
		try {
			String type = null;
			boolean isPrivate = false;
			boolean isPublic = false;
			boolean isStatic = false;
			for (ModifierContext modifier : ctx.modifier()) {
				if (modifier == null) {
					type = "private";
					isPrivate = true;
				} else if (modifier.classOrInterfaceModifier().PUBLIC() != null) {
					type = "public";
					isPublic = true;
				} else if (modifier.classOrInterfaceModifier().PRIVATE() != null) {
					type = "private";
					isPrivate = true;
				} else if (modifier.classOrInterfaceModifier().PROTECTED() != null) {
					type = "protected";
				} else if (modifier.classOrInterfaceModifier().STATIC() != null) {
					isStatic = true;
				}
			}
//			System.out.println(type);
			if (ctx.memberDeclaration() != null) {
				if (ctx.memberDeclaration().fieldDeclaration() != null) {
					// variable here
					String name = ctx.memberDeclaration().fieldDeclaration().variableDeclarators().variableDeclarator(0).variableDeclaratorId().getText();
//				System.out.println("var = " + type + " == " + name);

					ModuleLib.log("field: " + name + " > " + ctx.getStart().getStartIndex() + ", " + ctx.getStart().getLine());
					data.add(new Data(Type.variable, ctx.getStart().getStartIndex(), ctx.getStart().getLine(), name, isPublic, isPrivate, isStatic));
				} else if (ctx.memberDeclaration().methodDeclaration() != null) {
					String name = ctx.memberDeclaration().methodDeclaration().identifier().getText();
//				System.out.println("func = " + type + " == " + name);
					ModuleLib.log("method: " + name + " > " + ctx.getStart().getStartIndex() + ", " + ctx.getStart().getLine());
					data.add(new Data(Type.function, ctx.getStart().getStartIndex(), ctx.getStart().getLine(), name, isPublic, isPrivate, isStatic));
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

//	@Override
//	public void enterLocalVariableDeclaration(Java8Parser.LocalVariableDeclarationContext ctx) {
//		List<VariableModifierContext> variableModifierList = ctx.variableModifier();
//		boolean isPublic = false;
//		boolean isPrivate = false;
//		boolean isStatic = false;
//		for (VariableModifierContext m : variableModifierList) {
//			if (m.getText().equals("public")) {
//				isPublic = true;
//			} else if (m.getText().equals("private")) {
//				isPrivate = true;
//			} else if (m.getText().equals("static")) {
//				isStatic = true;
//			}
//		}
//		data.add(new Data(Type.variable, ctx.getStart().getStartIndex(), ctx.getStart().getLine(), ctx.variableDeclaratorList().variableDeclarator(0).variableDeclaratorId().Identifier().getText(), isPublic, isPrivate, isStatic));
//	}
//
//	@Override
//	public void enterMethodDeclaration(JavaParser.MethodDeclarationContext ctx) {
//		List<Java8Parser.MethodModifierContext> modifierList = ctx.typeTypeOrVoid().typeType().classOrInterfaceType().
//		boolean isPublic = false;
//		boolean isPrivate = false;
//		boolean isStatic = false;
//		for (MethodModifierContext m : modifierList) {
//			if (m.getText().equals("public")) {
//				isPublic = true;
//			} else if (m.getText().equals("private")) {
//				isPrivate = true;
//			} else if (m.getText().equals("static")) {
//				isStatic = true;
//			}
//		}
//
//		data.add(new Data(Type.function, ctx.getStart().getStartIndex(), ctx.getStart().getLine(), ctx.methodHeader().methodDeclarator().Identifier().getText(), isPublic, isPrivate, isStatic));
//	}
}
