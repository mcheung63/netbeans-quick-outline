package hk.quantr.netbeansquickoutline.antlr.listener;

import hk.quantr.netbeansquickoutline.Data;
import hk.quantr.netbeansquickoutline.Type;
import hk.quantr.netbeansquickoutline.antlr.CPP14BaseListener;
import hk.quantr.netbeansquickoutline.antlr.CPP14Parser;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.Token;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyCPP14Listener extends CPP14BaseListener {

	public ArrayList<Data> data = new ArrayList<Data>();

	private final Map<String, Integer> rmap = new HashMap<>();
	Predicate<String> filter = x -> !x.isEmpty();
	Parser parser;

	public MyCPP14Listener(Parser parser) {
		this.parser = parser;
		rmap.putAll(parser.getRuleIndexMap());
	}

	public String getRuleByKey(int key) {
		return rmap.entrySet().stream().filter(e -> Objects.equals(e.getValue(), key)).map(Map.Entry::getKey).findFirst().orElse(null);
	}

	@Override
	public void enterFunctiondefinition(CPP14Parser.FunctiondefinitionContext ctx) {
//		String ruleName = getRuleByKey(ctx.getRuleIndex());
		String functionName = ctx.declarator().getText();
		String declspecifierseq = ctx.declspecifierseq().getText();
//		System.out.println(ctx.attributespecifierseq().getText());
//		System.out.println(ctx.declspecifierseq().getText());
		Token s = ctx.getStart();
		Token e = ctx.getStop();

		data.add(new Data(Type.function, ctx.getStart().getStartIndex(), ctx.getStart().getLine(), declspecifierseq + " " + functionName, false, false, false));
	}

//	@Override
//	public void visitTerminal(TerminalNode tn) {
//		String temp = parser.getVocabulary().getSymbolicName(tn.getSymbol().getType());
//		data.add(new Data(Type.variable, tn.getSymbol().getStartIndex(), tn.getSymbol().getLine(), temp, false, false, false));
//	}
}
