package hk.quantr.netbeansquickoutline.antlr.listener;

import hk.quantr.netbeansquickoutline.Data;
import hk.quantr.netbeansquickoutline.Type;
import hk.quantr.netbeansquickoutline.antlr.JavaScriptParser;
import hk.quantr.netbeansquickoutline.antlr.JavaScriptParserBaseListener;
import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyJavascriptListener extends JavaScriptParserBaseListener {

	public ArrayList<Data> data = new ArrayList<Data>();

	@Override
	public void enterMethodDefinition(JavaScriptParser.MethodDefinitionContext ctx) {
		boolean isPublic = false;
		boolean isPrivate = false;
		boolean isStatic = false;
		data.add(new Data(Type.function, ctx.getStart().getStartIndex(), ctx.getStart().getLine(), ctx.propertyName().getText(), isPublic, isPrivate, isStatic));
	}
}
