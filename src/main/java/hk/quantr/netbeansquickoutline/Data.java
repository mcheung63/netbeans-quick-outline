package hk.quantr.netbeansquickoutline;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Data {

	public Type type;

	public int caretPosition;
	public int line;

	public String name;

	public boolean isPublic;
	public boolean isPrivate;
	public boolean isStatic;

	public Data(Type type, int caretPosition, int line, String name, boolean isPublic, boolean isPrivate, boolean isStatic) {
		this.type = type;
		this.caretPosition = caretPosition;
		this.line = line;
		this.name = name;
		this.isPublic = isPublic;
		this.isPrivate = isPrivate;
		this.isStatic = isStatic;
	}

	public String toString() {
		return type + " : " + line + " : " + name;
	}
}
