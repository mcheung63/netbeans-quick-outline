package hk.quantr.netbeansquickoutline;

import hk.quantr.netbeansquickoutline.antlr.ANTLRv4Lexer;
import hk.quantr.netbeansquickoutline.antlr.ANTLRv4Parser;
import hk.quantr.netbeansquickoutline.antlr.CLexer;
import hk.quantr.netbeansquickoutline.antlr.CPP14Lexer;
import hk.quantr.netbeansquickoutline.antlr.CPP14Parser;
import hk.quantr.netbeansquickoutline.antlr.CParser;
import hk.quantr.netbeansquickoutline.antlr.JavaLexer;
import hk.quantr.netbeansquickoutline.antlr.JavaParser;
import hk.quantr.netbeansquickoutline.antlr.listener.MyJavaListener;
import hk.quantr.netbeansquickoutline.antlr.JavaScriptLexer;
import hk.quantr.netbeansquickoutline.antlr.JavaScriptParser;
import hk.quantr.netbeansquickoutline.antlr.listener.MyAntlr4Listener;
import hk.quantr.netbeansquickoutline.antlr.listener.MyCListener;
import hk.quantr.netbeansquickoutline.antlr.listener.MyCPP14Listener;
import hk.quantr.netbeansquickoutline.antlr.listener.MyJavascriptListener;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.text.JTextComponent;
import org.antlr.v4.runtime.ANTLRInputStream;
//import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.netbeans.api.editor.EditorRegistry;
import org.openide.cookies.EditorCookie;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle.Messages;
import org.netbeans.modules.editor.NbEditorUtilities;
import org.openide.filesystems.FileObject;

@ActionID(
		category = "Navigate",
		id = "hk.quantr.netbeansquickoutline.QuickOutlineAction"
)
@ActionRegistration(
		displayName = "#CTL_QuickOutlineAction"
)
@ActionReferences({
	@ActionReference(path = "Menu/Navigate", position = 0),
	@ActionReference(path = "Shortcuts", name = "DO-P")
})
@Messages("CTL_QuickOutlineAction=Quick outline")
public final class QuickOutlineAction implements ActionListener {

	private final EditorCookie context;

	public QuickOutlineAction(EditorCookie context) {
		this.context = context;
	}

	@Override
	public void actionPerformed(ActionEvent ev) {

		JTextComponent jTextComponent = EditorRegistry.lastFocusedComponent();
		if (jTextComponent == null) {
			return;
		}
		try {
			FileObject fileObject = NbEditorUtilities.getFileObject(jTextComponent.getDocument());
			String filename = fileObject.getNameExt().toLowerCase();

			MyJDialog dialog = new MyJDialog(null, true);
			MyTableModel model = (MyTableModel) dialog.jTable1.getModel();

			if (filename.endsWith(".java")) {
				JavaLexer lexer = new JavaLexer(new ANTLRInputStream(jTextComponent.getText()));
				CommonTokenStream tokenStream = new CommonTokenStream(lexer);
				JavaParser parser = new JavaParser(tokenStream);
				ParserRuleContext t = parser.compilationUnit();

				ParseTreeWalker walker = new ParseTreeWalker();
				MyJavaListener listener = new MyJavaListener();
				walker.walk(listener, t);

				model.data = listener.data;
			} else if (filename.endsWith(".c")) {
				CLexer lexer = new CLexer(new ANTLRInputStream(jTextComponent.getText()));
				CommonTokenStream tokenStream = new CommonTokenStream(lexer);
				CParser parser = new CParser(tokenStream);
				ParserRuleContext t = parser.primaryExpression();

				ParseTreeWalker walker = new ParseTreeWalker();
				MyCListener listener = new MyCListener(parser);
				walker.walk(listener, t);

				model.data = listener.data;
			} else if (filename.endsWith(".cpp")) {
				CPP14Lexer lexer = new CPP14Lexer(new ANTLRInputStream(jTextComponent.getText()));
				CommonTokenStream tokenStream = new CommonTokenStream(lexer);
				CPP14Parser parser = new CPP14Parser(tokenStream);
				ParserRuleContext t = parser.translationunit();

				ParseTreeWalker walker = new ParseTreeWalker();
				MyCPP14Listener listener = new MyCPP14Listener(parser);
				walker.walk(listener, t);

				model.data = listener.data;
			} else if (filename.endsWith(".js")) {
				JavaScriptLexer lexer = new JavaScriptLexer(new ANTLRInputStream(jTextComponent.getText()));
				CommonTokenStream tokenStream = new CommonTokenStream(lexer);
				JavaScriptParser parser = new JavaScriptParser(tokenStream);
				ParserRuleContext t = parser.program();

				ParseTreeWalker walker = new ParseTreeWalker();
				MyJavascriptListener listener = new MyJavascriptListener();
				walker.walk(listener, t);

				model.data = listener.data;
			} else if (filename.endsWith(".g4")) {
//				String str = IOUtils.toString(new FileInputStream("/Users/peter/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerLexer.g4"));
				String str = jTextComponent.getText();

				ANTLRv4Lexer lexer = new ANTLRv4Lexer(new ANTLRInputStream(str));
				CommonTokenStream tokenStream = new CommonTokenStream(lexer);
				ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
				ParserRuleContext t = parser.grammarSpec();

				ParseTreeWalker walker = new ParseTreeWalker();
				MyAntlr4Listener listener = new MyAntlr4Listener(str, parser);
				walker.walk(listener, t);

				model.data = listener.data;
			} else {
				return;
			}

			model.fireTableDataChanged();
//			dialog.jTable1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//			dialog.jTable1.getColumnModel().getColumn(0).setWidth(200);
//			dialog.jTable1.getColumnModel().getColumn(1).setWidth(20);
//			CommonLib.autoResizeColumn(dialog.jTable1);

			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			dialog.setSize((int) screenSize.getWidth() * 2 / 3, (int) screenSize.getHeight() * 2 / 3);

//			dialog.jTable1.getColumnModel().getColumn(0).setPreferredWidth(50);
//			dialog.jTable1.getColumnModel().getColumn(1).setPreferredWidth(50);
//			dialog.jTable1.getColumnModel().getColumn(2).setPreferredWidth(dialog.getWidth() - 100);
			dialog.setLocationRelativeTo(null);
			dialog.setVisible(true);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
