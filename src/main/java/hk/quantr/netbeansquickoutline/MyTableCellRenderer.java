/*
 * Copyright (C) 2018 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hk.quantr.netbeansquickoutline;

import hk.quantr.javalib.CommonLib;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyTableCellRenderer implements TableCellRenderer {
	
	Color functionBG = Color.decode("#ffeef5");
	Color variableBG = Color.decode("#d8f0ff");
	
	ImageIcon functionIcon = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getClassLoader().getResource("hk/quantr/netbeansquickoutline/function.png")), 12, 12, Image.SCALE_SMOOTH);
	ImageIcon variableIcon = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getClassLoader().getResource("hk/quantr/netbeansquickoutline/variable.png")), 12, 12, Image.SCALE_SMOOTH);
	ImageIcon publicIcon = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getClassLoader().getResource("hk/quantr/netbeansquickoutline/public.png")), 45, 12, Image.SCALE_SMOOTH);
	ImageIcon privateIcon = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getClassLoader().getResource("hk/quantr/netbeansquickoutline/private.png")), 45, 12, Image.SCALE_SMOOTH);
	ImageIcon staticIcon = CommonLib.resizeImageToIcon(new ImageIcon(getClass().getClassLoader().getResource("hk/quantr/netbeansquickoutline/static.png")), 45, 12, Image.SCALE_SMOOTH);
	JLabel label = new JLabel();
	JPanel panel = new JPanel();
	JLabel mainIconLabel = new JLabel();
	JLabel publicIconLabel = new JLabel();
	JLabel privateIconLabel = new JLabel();
	JLabel staticIconLabel = new JLabel();
//	JLabel textLabel = new JLabel();

	public MyTableCellRenderer() {
		label.setOpaque(true);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 1));
//		panel.setBorder(new EmptyBorder(0, 0, 0, 0));
		mainIconLabel.setPreferredSize(new Dimension(280, 25));
		panel.add(mainIconLabel);
//		publicIconLabel.setPreferredSize(new Dimension(45, 25));
//		privateIconLabel.setPreferredSize(new Dimension(45, 25));
//		staticIconLabel.setPreferredSize(new Dimension(45, 25));
		panel.add(publicIconLabel);
		panel.add(privateIconLabel);
		panel.add(staticIconLabel);
//		panel.add(textLabel);
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (column == 0) {
			Data data = (Data) table.getValueAt(row, 0);
			if (data.type == Type.function) {
				mainIconLabel.setIcon(functionIcon);
			} else if (data.type == Type.variable) {
				mainIconLabel.setIcon(variableIcon);
			} else {
				mainIconLabel.setIcon(null);
			}
			publicIconLabel.setIcon(null);
			if (data.isPublic) {
				publicIconLabel.setIcon(publicIcon);
			} else {
				publicIconLabel.setIcon(null);
			}
			if (data.isPrivate) {
				privateIconLabel.setIcon(privateIcon);
			} else {
				privateIconLabel.setIcon(null);
			}
			if (data.isStatic) {
				staticIconLabel.setIcon(staticIcon);
			} else {
				staticIconLabel.setIcon(null);
			}
			
			if (isSelected) {
				panel.setForeground(table.getSelectionForeground());
				panel.setBackground(table.getSelectionBackground());
				mainIconLabel.setForeground(table.getSelectionForeground());
			} else {
				panel.setForeground(table.getForeground());
				mainIconLabel.setForeground(table.getForeground());
				if (data.type == Type.function) {
					panel.setBackground(functionBG);
				} else if (data.type == Type.variable) {
					panel.setBackground(variableBG);
				} else {
					panel.setBackground(table.getBackground());
				}
			}
			if (data.type == Type.function) {
				mainIconLabel.setText("function");
			} else if (data.type == Type.variable) {
				mainIconLabel.setText("variable");
			} else if (data.type == Type.lexer) {
				mainIconLabel.setText("lexer");
			} else if (data.type == Type.parser) {
				mainIconLabel.setText("parser");
			} else {
				mainIconLabel.setText(null);
			}
			
			return panel;
		} else {
			if (isSelected) {
				label.setForeground(table.getSelectionForeground());
				label.setBackground(table.getSelectionBackground());
			} else {
				label.setForeground(table.getForeground());
				Data data = (Data) table.getValueAt(row, 0);
				if (data.type == Type.function) {
					label.setBackground(functionBG);
				} else if (data.type == Type.variable) {
					label.setBackground(variableBG);
				} else {
					label.setBackground(table.getBackground());
				}
			}
			label.setText(value.toString());
			return label;
		}
	}
}
