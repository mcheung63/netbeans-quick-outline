//package hk.quantr.netbeansquickoutline;
//
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import javax.swing.JOptionPane;
//import org.openide.awt.ActionID;
//import org.openide.awt.ActionReference;
//import org.openide.awt.ActionReferences;
//import org.openide.awt.ActionRegistration;
//import org.openide.util.NbBundle.Messages;
//
//@ActionID(
//		category = "Shortcuts",
//		id = "hk.quantr.netbeansquickoutline.SomeAction"
//)
//@ActionRegistration(
//		displayName = "#CTL_SomeAction"
//)
//@ActionReferences({
//	@ActionReference(path = "Menu/Shortcuts")
//	,
//	@ActionReference(path = "Shortcuts", name = "M-P")
//})
//@Messages("CTL_SomeAction=MyMenu")
//public final class SomeAction implements ActionListener {
//
//	@Override
//	public void actionPerformed(ActionEvent e) {
//		JOptionPane.showMessageDialog(null, "hey");
//	}
//}
