/*
 * Copyright (C) 2021 Peter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hk.quantr.netbeans.quickoutline;

import hk.quantr.netbeansquickoutline.MyJDialog;
import hk.quantr.netbeansquickoutline.MyTableModel;
import hk.quantr.netbeansquickoutline.antlr.ANTLRv4Lexer;
import hk.quantr.netbeansquickoutline.antlr.ANTLRv4Parser;
import hk.quantr.netbeansquickoutline.antlr.CLexer;
import hk.quantr.netbeansquickoutline.antlr.CPP14Lexer;
import hk.quantr.netbeansquickoutline.antlr.CPP14Parser;
import hk.quantr.netbeansquickoutline.antlr.CParser;
import hk.quantr.netbeansquickoutline.antlr.JavaLexer;
import hk.quantr.netbeansquickoutline.antlr.JavaParser;
import hk.quantr.netbeansquickoutline.antlr.JavaScriptLexer;
import hk.quantr.netbeansquickoutline.antlr.JavaScriptParser;
import hk.quantr.netbeansquickoutline.antlr.listener.MyAntlr4Listener;
import hk.quantr.netbeansquickoutline.antlr.listener.MyCListener;
import hk.quantr.netbeansquickoutline.antlr.listener.MyCPP14Listener;
import hk.quantr.netbeansquickoutline.antlr.listener.MyJavaListener;
import hk.quantr.netbeansquickoutline.antlr.listener.MyJavascriptListener;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter
 */
public class TestDialog {

	@Test
	public void test() throws FileNotFoundException, IOException {
		String filename = "MyJDialog.java";
		String content = IOUtils.toString(new FileInputStream("C:\\workspace\\netbeans-quick-outline\\src\\main\\java\\hk\\quantr\\netbeansquickoutline\\MyJDialog.java"), "utf8");

		MyJDialog dialog = new MyJDialog(null, true);
		MyTableModel model = (MyTableModel) dialog.jTable1.getModel();

		if (filename.endsWith(".java")) {
			JavaLexer lexer = new JavaLexer(new ANTLRInputStream(content));
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			JavaParser parser = new JavaParser(tokenStream);
			ParserRuleContext t = parser.compilationUnit();

			ParseTreeWalker walker = new ParseTreeWalker();
			MyJavaListener listener = new MyJavaListener();
			walker.walk(listener, t);

			model.data = listener.data;
		} else if (filename.endsWith(".c")) {
			CLexer lexer = new CLexer(new ANTLRInputStream(content));
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			CParser parser = new CParser(tokenStream);
			ParserRuleContext t = parser.primaryExpression();

			ParseTreeWalker walker = new ParseTreeWalker();
			MyCListener listener = new MyCListener(parser);
			walker.walk(listener, t);

			model.data = listener.data;
		} else if (filename.endsWith(".cpp")) {
			CPP14Lexer lexer = new CPP14Lexer(new ANTLRInputStream(content));
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			CPP14Parser parser = new CPP14Parser(tokenStream);
			ParserRuleContext t = parser.translationunit();

			ParseTreeWalker walker = new ParseTreeWalker();
			MyCPP14Listener listener = new MyCPP14Listener(parser);
			walker.walk(listener, t);

			model.data = listener.data;
		} else if (filename.endsWith(".js")) {
			JavaScriptLexer lexer = new JavaScriptLexer(new ANTLRInputStream(content));
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			JavaScriptParser parser = new JavaScriptParser(tokenStream);
			ParserRuleContext t = parser.program();

			ParseTreeWalker walker = new ParseTreeWalker();
			MyJavascriptListener listener = new MyJavascriptListener();
			walker.walk(listener, t);

			model.data = listener.data;
		} else if (filename.endsWith(".g4")) {
//				String str = IOUtils.toString(new FileInputStream("/Users/peter/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerLexer.g4"));
			String str = content;

			ANTLRv4Lexer lexer = new ANTLRv4Lexer(new ANTLRInputStream(str));
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
			ParserRuleContext t = parser.grammarSpec();

			ParseTreeWalker walker = new ParseTreeWalker();
			MyAntlr4Listener listener = new MyAntlr4Listener(str, parser);
			walker.walk(listener, t);

			model.data = listener.data;
		} else {
			return;
		}

		model.fireTableDataChanged();
//			dialog.jTable1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//			dialog.jTable1.getColumnModel().getColumn(0).setWidth(200);
//			dialog.jTable1.getColumnModel().getColumn(1).setWidth(20);
//			CommonLib.autoResizeColumn(dialog.jTable1);

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		dialog.setSize((int) screenSize.getWidth() * 2 / 3, (int) screenSize.getHeight() * 2 / 3);

//			dialog.jTable1.getColumnModel().getColumn(0).setPreferredWidth(50);
//			dialog.jTable1.getColumnModel().getColumn(1).setPreferredWidth(50);
//			dialog.jTable1.getColumnModel().getColumn(2).setPreferredWidth(dialog.getWidth() - 100);
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);
	}
}
