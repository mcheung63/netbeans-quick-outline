/*
 * Copyright (C) 2018 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hk.quantr.netbeans.quickoutline;

import hk.quantr.netbeansquickoutline.antlr.CLexer;
import hk.quantr.netbeansquickoutline.antlr.CParser;
import hk.quantr.netbeansquickoutline.antlr.listener.MyCListener;
import java.io.FileInputStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestC {

	@Test
	public void test() throws Exception {
//		MyJDialog dialog = new MyJDialog(null, true);
//		MyTableModel model = (MyTableModel) dialog.jTable1.getModel();
		CLexer lexer = new CLexer(new ANTLRInputStream(IOUtils.toString(new FileInputStream("/Users/peter/Desktop/b.c"), "utf-8")));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		CParser parser = new CParser(tokenStream);
		ParserRuleContext t = parser.primaryExpression();

		ParseTreeWalker walker = new ParseTreeWalker();
		MyCListener listener = new MyCListener(parser);
		walker.walk(listener, t);

//		model.data = listener.data;
//
//		model.fireTableDataChanged();
//		CommonLib.autoResizeColumn(dialog.jTable1);
//
//		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
//		dialog.setSize((int) screenSize.getWidth() * 2 / 3, (int) screenSize.getHeight() * 2 / 3);
//		dialog.setLocationRelativeTo(null);
//		dialog.setVisible(true);
	}
}
