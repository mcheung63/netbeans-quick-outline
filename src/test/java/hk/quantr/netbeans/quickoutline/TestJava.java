package hk.quantr.netbeans.quickoutline;

import hk.quantr.netbeansquickoutline.Data;
import hk.quantr.netbeansquickoutline.antlr.JavaLexer;
import hk.quantr.netbeansquickoutline.antlr.JavaParser;
import hk.quantr.netbeansquickoutline.antlr.listener.MyJavaListener;
import java.io.FileInputStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RuntimeMetaData;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestJava {

	@Test
	public void test() throws Exception {
		System.out.println("start antlr=" + RuntimeMetaData.VERSION);
//		JavaLexer lexer = new JavaLexer(new ANTLRInputStream(IOUtils.toString(new FileInputStream("/Users/peter/workspace/pfsbuilder/src/main/java/com/pfsbuilder/MainFrame2.java"), "utf-8")));
		JavaLexer lexer = new JavaLexer(CharStreams.fromString(IOUtils.toString(new FileInputStream("testSource/Simulator.java"), "utf-8")));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		JavaParser parser = new JavaParser(tokenStream);
		ParserRuleContext t = parser.compilationUnit();
		System.out.println("start ParseTreeWalker");
		ParseTreeWalker walker = new ParseTreeWalker();
		MyJavaListener listener = new MyJavaListener();
		System.out.println("start walk");
		walker.walk(listener, t);
		System.out.println("-".repeat(100));
		for (Data data : listener.data) {
			System.out.println(data);
		}
	}
}
