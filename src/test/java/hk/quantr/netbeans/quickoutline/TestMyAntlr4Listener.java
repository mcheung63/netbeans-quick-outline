package hk.quantr.netbeans.quickoutline;

import hk.quantr.netbeansquickoutline.antlr.listener.*;
import hk.quantr.netbeansquickoutline.Data;
import hk.quantr.netbeansquickoutline.ModuleLib;
import hk.quantr.netbeansquickoutline.Type;
import hk.quantr.netbeansquickoutline.antlr.ANTLRv4ParserBaseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestMyAntlr4Listener extends ANTLRv4ParserBaseListener {

	public String str;
	public ArrayList<Data> data = new ArrayList<Data>();

	private final Map<String, Integer> rmap = new HashMap<>();
	Predicate<String> filter = x -> !x.isEmpty();
	Parser parser;

	public TestMyAntlr4Listener(String str, Parser parser) {
		this.str = str;
		this.parser = parser;
		rmap.putAll(parser.getRuleIndexMap());
	}

	public String getRuleByKey(int key) {
		return rmap.entrySet().stream().filter(e -> Objects.equals(e.getValue(), key)).map(Map.Entry::getKey).findFirst().orElse(null);
	}

	@Override
	public void enterEveryRule(ParserRuleContext ctx) {
		String ruleName = getRuleByKey(ctx.getRuleIndex());
		System.out.println("ruleName =" + ruleName + "\t\t" + ctx.getText());
		String text = ctx.getText();
		Token s = ctx.getStart();
		Token e = ctx.getStop();
		System.out.println(s.getStartIndex() + " > " + e.getStopIndex());
		String realText;
		if (s.getStartIndex() < e.getStopIndex()) {
			realText = str.substring(s.getStartIndex(), e.getStopIndex());
		} else {
			realText = str.substring(e.getStopIndex(), s.getStartIndex());
		}
//
//		if (ruleName.equals("sourceElement")) {
//			data.add(new Data(Type.function, ctx.getStart().getStartIndex(), ctx.getStart().getLine(), text, false, false, false));
//		} else if (ruleName.equals("lexerRuleSpec")) {
//			data.add(new Data(Type.function, ctx.getStart().getStartIndex(), ctx.getStart().getLine(), text.split(":")[0], false, false, false));
//		}

		if (ruleName.equals("lexerRuleSpec")) {
			data.add(new Data(Type.lexer, ctx.getStart().getStartIndex(), ctx.getStart().getLine(), realText.split(":")[0], false, false, false));
		} else if (ruleName.equals("parserRuleSpec")) {
			data.add(new Data(Type.parser, ctx.getStart().getStartIndex(), ctx.getStart().getLine(), realText.split(":")[0], false, false, false));
		}
	}

//	@Override
//	public void visitTerminal(TerminalNode tn) {
//		String temp = parser.getVocabulary().getSymbolicName(tn.getSymbol().getType());
//		data.add(new Data(Type.variable, tn.getSymbol().getStartIndex(), tn.getSymbol().getLine(), temp, false, false, false));
//	}
}
