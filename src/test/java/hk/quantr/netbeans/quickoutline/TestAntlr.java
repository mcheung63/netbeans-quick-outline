/*
 * Copyright (C) 2018 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hk.quantr.netbeans.quickoutline;

import hk.quantr.javalib.CommonLib;
import hk.quantr.netbeansquickoutline.MyJDialog;
import hk.quantr.netbeansquickoutline.MyTableModel;
import hk.quantr.netbeansquickoutline.antlr.ANTLRv4Lexer;
import hk.quantr.netbeansquickoutline.antlr.ANTLRv4Parser;
import hk.quantr.netbeansquickoutline.antlr.listener.MyAntlr4Listener;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileInputStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestAntlr {

	@Test
	public void test() throws Exception {
		boolean showDialog = true;

		MyJDialog dialog = null;
		MyTableModel model = null;
		if (showDialog) {
			dialog = new MyJDialog(null, true);
			model = (MyTableModel) dialog.jTable1.getModel();
		}

		String str = IOUtils.toString(new FileInputStream("/Users/peter/workspace/Assembler/src/main/java/hk/quantr/assembler/antlr/AssemblerLexer.g4"));
		ANTLRv4Lexer lexer = new ANTLRv4Lexer(new ANTLRInputStream(str));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
		ParserRuleContext t = parser.grammarSpec();

		ParseTreeWalker walker = new ParseTreeWalker();
		MyAntlr4Listener listener = new MyAntlr4Listener(str, parser);
		walker.walk(listener, t);

		if (showDialog) {
			model.data = listener.data;

			model.fireTableDataChanged();
			CommonLib.autoResizeColumn(dialog.jTable1);

			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			dialog.setSize((int) screenSize.getWidth() * 2 / 3, (int) screenSize.getHeight() * 2 / 3);
			dialog.setLocationRelativeTo(null);
			dialog.setVisible(true);
		}
	}
}
