/*
 * Copyright (C) 2018 Peter <peter@quantr.hk>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hk.quantr.netbeans.quickoutline;

import hk.quantr.netbeansquickoutline.Data;
import hk.quantr.netbeansquickoutline.antlr.CPP14Lexer;
import hk.quantr.netbeansquickoutline.antlr.CPP14Parser;
import hk.quantr.netbeansquickoutline.antlr.listener.MyCPP14Listener;
import java.io.FileInputStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestCPP14 {

	@Test
	public void test() throws Exception {
		CPP14Lexer lexer = new CPP14Lexer(new ANTLRInputStream(IOUtils.toString(new FileInputStream("/Users/peter/workspace/PeterI/kernel/kernel.cpp"), "utf-8")));

		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		CPP14Parser parser = new CPP14Parser(tokenStream);
		ParserRuleContext t = parser.translationunit();

		ParseTreeWalker walker = new ParseTreeWalker();
		MyCPP14Listener listener = new MyCPP14Listener(parser);
		walker.walk(listener, t);

		System.out.println("----------------");
		System.out.println(listener.data.size());
		for (Data data : listener.data) {
			System.out.println(data);
		}
	}
}
